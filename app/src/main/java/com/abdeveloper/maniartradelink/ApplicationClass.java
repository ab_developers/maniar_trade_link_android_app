package com.abdeveloper.maniartradelink;

import android.app.Application;
import android.support.v4.app.Fragment;

import com.abdeveloper.maniartradelink.retrofit.Constants;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.threetenabp.AndroidThreeTen;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by root on 3/1/19.
 */

public class ApplicationClass extends Application {

    public static String token;
    public static String CONTAINER_TITLE = "Details";
    public static Fragment CONTAINERFRAG;
    public static String CONTAINERFRAGTAG = "";
    @Override
    public void onCreate() {
        super.onCreate();

        AndroidThreeTen.init(this);


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Blogger_Sans.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        // Initialize Places.
        Places.initialize(getApplicationContext(), Constants.API_KEY);

// Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);
    }
}
