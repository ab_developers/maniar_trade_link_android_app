package com.abdeveloper.maniartradelink;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.abdeveloper.maniartradelink.utils.drawer.MaterialDrawer;
import com.abdeveloper.maniartradelink.utils.storage.MySharedPreferences;
import com.mikepenz.materialdrawer.DrawerBuilder;


import butterknife.BindView;
import butterknife.ButterKnife;

public class OpenContainer extends BaseActivity {

    public static final int COMMON_MENU = 0;
    private Intent bundle;
    private String TAG="PDF";



    @BindView(R.id.toolbar)
    Toolbar toolbar;

    MySharedPreferences sharedPreferences;

    MaterialDrawer drawer;
    //Activities all view


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onResume() {
        super.onResume();

        if(drawer!=null){
            drawer.setUpDrawer();
        }

        toolbar.getMenu().clear();
        int toolbarCategory = bundle.getIntExtra("toolbarCategory", -1);
        switch (toolbarCategory){
            case COMMON_MENU:
                //toolbar.inflateMenu(R.menu.common_menu);
                //setting up just empty_cart on menu_item
                //StaticFunctions.setUpCartAndSearchOnToolbar(toolbar, searchView,this,toolbar.getMenu().findItem(R.id.menu_cart),toolbar.getMenu().findItem(R.id.action_search),sharedPreferences.getCartLength());

                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.open_container_layout);
        ButterKnife.bind(this);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        //getting bundle
        bundle = getIntent();

        sharedPreferences = new MySharedPreferences(OpenContainer.this);


        toolbar.setNavigationIcon(R.drawable.ic_arrow_white_24dp);
        toolbar.setTitle(ApplicationClass.CONTAINER_TITLE);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(bundle!=null) {
            if (bundle.getStringExtra("show_drawer") != null) {
                if (bundle.getStringExtra("show_drawer").equals("yes")) {
                    //adding drawer
                    new DrawerBuilder().withActivity(this).build();
                    drawer = new MaterialDrawer(OpenContainer.this, toolbar);
                    drawer.setUpDrawer();
                }
            }

        }

        //productSearch


        try {
            fragmentTransaction.replace(R.id.content_main, ApplicationClass.CONTAINERFRAG,ApplicationClass.CONTAINERFRAGTAG).commit();
        } catch (Exception e) {

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==11){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED ){
             //   printFromServer();
            }else{
                Toast.makeText(OpenContainer.this,"Please grant the permission to use this feature",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        //For closing sea
    }


}
