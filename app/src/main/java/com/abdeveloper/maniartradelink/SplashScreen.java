package com.abdeveloper.maniartradelink;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


import com.abdeveloper.maniartradelink.auth.LoginActivity;
import com.abdeveloper.maniartradelink.home.DashboardActivity;
import com.abdeveloper.maniartradelink.utils.animation.CustomAnimations;
import com.abdeveloper.maniartradelink.utils.storage.MySharedPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by root on 3/1/19.
 */

public class SplashScreen extends BaseActivity {

    //Bitmap backgroundImage;

    @BindView(R.id.background_image)
    ImageView background_image;

    @BindView(R.id.app_icon)
    ImageView app_icon;

    @BindView(R.id.app_title)
    TextView app_title;

    private boolean mShouldFinish = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen_layout);

        ButterKnife.bind(this);

        //backgroundImage = StaticFunctions.decodeSampledBitmapFromResource(getResources(), R.drawable.background_img_vertical,this);
        //background_image.setImageBitmap(backgroundImage);

        app_title.setAnimation( AnimationUtils.loadAnimation(this, R.anim.fadein));
        app_icon.setAnimation( AnimationUtils.loadAnimation(this, R.anim.fadein));

        //Animations
        animations();

        if(!new MySharedPreferences(SplashScreen.this).getToken().equals("")){
            Handler handler = new Handler();
            Runnable r = new Runnable() {
                public void run() {
                    Intent intent = new Intent(SplashScreen.this, DashboardActivity.class);
                    startActivity(intent);
                    finish();

                }
            };
            handler.postDelayed(r, 1000);


        }else{
            Handler handler = new Handler();
            Runnable r = new Runnable() {
                public void run() {
                    Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(SplashScreen.this, (View)app_icon, "app_logo");
                    startActivity(intent, options.toBundle());

                    mShouldFinish = true;
                }
            };
            handler.postDelayed(r, 2000);
        }


    }

    private void animations() {
        new CustomAnimations().splashScreenAnimatino(app_icon,app_title);
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mShouldFinish)
            finish();
    }
}
