package com.abdeveloper.maniartradelink.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.abdeveloper.maniartradelink.BaseActivity;
import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by root on 5/2/19.
 */

public class LoginActivity extends BaseActivity {

    @BindView(R.id.login)
    Button login;

    @BindView(R.id.username)
    MaterialEditText username;

    @BindView(R.id.password)
    MaterialEditText password;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        ButterKnife.bind(this);

        //Normal Login
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!username.getText().toString().equals("") && !password.getText().toString().equals("")) {

                    if (StaticFunctions.isOnline(LoginActivity.this)) {
                        new ServiceAPI(LoginActivity.this).postLogin(username.getText().toString(), password.getText().toString());

                    } else {
                        StaticFunctions.internetConnectionIssue(LoginActivity.this);
                    }
                }
            }
        });

    }
}
