package com.abdeveloper.maniartradelink.auth.model;

/**
 * Created by root on 5/2/19.
 */

public class PostLoginModel {

    String username;
    String password;

    public PostLoginModel() {
    }

    public PostLoginModel(String login, String password) {
        this.username = login;
        this.password = password;
    }

    public String getLogin() {
        return username;
    }

    public void setLogin(String login) {
        this.username = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
