package com.abdeveloper.maniartradelink.auth.model;

/**
 * Created by root on 5/2/19.
 */

public class ResponseToken {

    String token;

    public ResponseToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
