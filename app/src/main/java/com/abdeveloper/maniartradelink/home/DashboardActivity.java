package com.abdeveloper.maniartradelink.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.abdeveloper.maniartradelink.BaseActivity;
import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.adapter.DashboardAdapter;
import com.abdeveloper.maniartradelink.home.model.DashboardItemListModel;
import com.abdeveloper.maniartradelink.utils.drawer.MaterialDrawer;
import com.mikepenz.materialdrawer.DrawerBuilder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by root on 5/2/19.
 */

public class DashboardActivity extends BaseActivity {

    MaterialDrawer drawer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.item_list_recyclerview)
    RecyclerView item_list_recyclerview;

    ArrayList<DashboardItemListModel> dashboardItemListModels = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_layout);

        ButterKnife.bind(this);

        toolbar.setTitle(R.string.drawer_item_dashboard);
        toolbar.setTitleTextColor(ContextCompat.getColor(this,R.color.white));

        //adding drawer
        new DrawerBuilder().withActivity(this).build();
        drawer = new MaterialDrawer(DashboardActivity.this, toolbar);

        drawer.setUpDrawer();


        setUpDashboard();

    }

    private void setUpDashboard() {
        dashboardItemListModels.clear();
        dashboardItemListModels.add(new DashboardItemListModel("Pending Orders",ContextCompat.getDrawable(this,R.drawable.ic_user)));
        dashboardItemListModels.add(new DashboardItemListModel("Dispatched Orders",ContextCompat.getDrawable(this,R.drawable.ic_user)));
        dashboardItemListModels.add(new DashboardItemListModel("Employee Schedule",ContextCompat.getDrawable(this,R.drawable.ic_user)));
        dashboardItemListModels.add(new DashboardItemListModel("Stocks",ContextCompat.getDrawable(this,R.drawable.ic_user)));


        GridLayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        item_list_recyclerview.setLayoutManager(mLayoutManager);
        DashboardAdapter dashboardAdapter = new DashboardAdapter(DashboardActivity.this, dashboardItemListModels);
        item_list_recyclerview.setAdapter(dashboardAdapter);
    }
}
