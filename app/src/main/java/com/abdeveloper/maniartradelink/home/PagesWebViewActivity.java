package com.abdeveloper.maniartradelink.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.abdeveloper.maniartradelink.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by root on 23/2/19.
 */

public class PagesWebViewActivity extends AppCompatActivity {

    @BindView(R.id.webView1)
    WebView webView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pages_webview_layout);

        ButterKnife.bind(this);

        webView.getSettings().setJavaScriptEnabled(true);
        toolbar.setNavigationIcon(ContextCompat.getDrawable(this,R.drawable.ic_arrow_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // TODO Auto-generated method stub
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }
        });

        Intent intent = getIntent();
        if(intent.getStringExtra("url")!=null) {
            toolbar.setTitleTextColor(ContextCompat.getColor(PagesWebViewActivity.this,R.color.white));
            toolbar.setTitle(intent.getStringExtra("title"));
            webView.loadUrl(intent.getStringExtra("url"));
        }else{
            finish();
        }
    }




}
