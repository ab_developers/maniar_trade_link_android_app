package com.abdeveloper.maniartradelink.home.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abdeveloper.maniartradelink.ApplicationClass;
import com.abdeveloper.maniartradelink.OpenContainer;
import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.fragment.FragmentStocks;
import com.abdeveloper.maniartradelink.home.model.DashboardItemListModel;
import com.abdeveloper.maniartradelink.meeting.fragment.FragmentCreateMeeting;
import com.abdeveloper.maniartradelink.meeting.fragment.FragmentMeetingList;
import com.abdeveloper.maniartradelink.orders.fragment.FragmentOrderList;
import com.abdeveloper.maniartradelink.retrofit.Constants;

import java.util.ArrayList;

/**
 * Created by root on 5/2/19.
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {

    private AppCompatActivity mActivity;
    private ArrayList<DashboardItemListModel> mlist;


    public DashboardAdapter(AppCompatActivity mActivity, ArrayList<DashboardItemListModel> cartModelArrayList) {
        this.mActivity = mActivity;
        this.mlist = cartModelArrayList;
    }

    @Override
    public DashboardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_dashboard_layout, parent, false);


        return new DashboardAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DashboardAdapter.MyViewHolder holder, final int position) {
        holder.item_title.setText(mlist.get(position).getTitle());
        holder.item_image.setImageDrawable(mlist.get(position).getImage());

        holder.main_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (position){
                    case 0:
                        FragmentOrderList fragmentOrderList = new FragmentOrderList();
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.ORDER_STATUS,Constants.PENDING);
                        fragmentOrderList.setArguments(bundle);
                        ApplicationClass.CONTAINERFRAG = fragmentOrderList;
                        ApplicationClass.CONTAINER_TITLE = mActivity.getString(R.string.pending_orders);
                        ApplicationClass.CONTAINERFRAGTAG = Constants.FRAGMENT_PEDNING_ORDERS_TAG;


                        Intent intent1 = new Intent(mActivity, OpenContainer.class);
                        intent1.putExtra("toolbarCategory", OpenContainer.COMMON_MENU);
                        mActivity.startActivity(intent1);
                        break;

                    case 1:
                        FragmentOrderList fragmentDispatchOrderList = new FragmentOrderList();
                        Bundle bundle1 = new Bundle();
                        bundle1.putString(Constants.ORDER_STATUS,Constants.DISPATCH);
                        fragmentDispatchOrderList.setArguments(bundle1);

                        ApplicationClass.CONTAINERFRAG = fragmentDispatchOrderList;
                        ApplicationClass.CONTAINER_TITLE = mActivity.getString(R.string.dispatch_orders);
                        ApplicationClass.CONTAINERFRAGTAG = Constants.FRAGMENT_DISPATCH_ORDERS_ORDERS_TAG;


                        Intent intent2 = new Intent(mActivity, OpenContainer.class);
                        intent2.putExtra("toolbarCategory", OpenContainer.COMMON_MENU);
                        mActivity.startActivity(intent2);

                        break;
                    case 2:
                        ApplicationClass.CONTAINERFRAG = new FragmentMeetingList();
                        ApplicationClass.CONTAINER_TITLE = mActivity.getString(R.string.drawer_item_meetings);
                        ApplicationClass.CONTAINERFRAGTAG = Constants.FRAGMENT_MEETINGS;


                        Intent intent5= new Intent(mActivity, OpenContainer.class);
                        intent5.putExtra("toolbarCategory", OpenContainer.COMMON_MENU);
                        mActivity.startActivity(intent5);

                        break;
                    case 3:
                        FragmentStocks fragmentStocks = new FragmentStocks();

                        ApplicationClass.CONTAINERFRAG = fragmentStocks;
                        ApplicationClass.CONTAINER_TITLE = mActivity.getString(R.string.drawer_item_stocks);
                        ApplicationClass.CONTAINERFRAGTAG = Constants.FRAGMENT_STOCKS_TAG;


                        Intent intent = new Intent(mActivity, OpenContainer.class);
                        intent.putExtra("toolbarCategory", OpenContainer.COMMON_MENU);
                        mActivity.startActivity(intent);
                        break;
                }
            }
        });


    }



    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView item_title;
        ImageView item_image;
        CardView main_container;

        public MyViewHolder(View view) {
            super(view);
            item_title = view.findViewById(R.id.item_title);
            item_image = view.findViewById(R.id.item_image);
            main_container = view.findViewById(R.id.main_container);

        }
    }


}
