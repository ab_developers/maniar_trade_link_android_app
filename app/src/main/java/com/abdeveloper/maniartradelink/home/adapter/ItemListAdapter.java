package com.abdeveloper.maniartradelink.home.adapter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;

import java.util.ArrayList;

/**
 * Created by root on 5/2/19.
 */

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.MyViewHolder> {

    private AppCompatActivity mActivity;
    private ArrayList<ItemModel> mlist;

    public ItemListAdapter(AppCompatActivity mActivity, ArrayList<ItemModel> cartModelArrayList) {
        this.mActivity = mActivity;
        this.mlist = cartModelArrayList;
    }

    @Override
    public ItemListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_layout, parent, false);
        return new ItemListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final ItemListAdapter.MyViewHolder holder, final int position) {

        StringBuilder total_items = new StringBuilder();
        total_items.append(mlist.get(position).getStock()+" x "+mlist.get(position).getName()+"\n");

        holder.total_items.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item)+" ",mActivity),"\n",StaticFunctions.setSpannableText(total_items.toString()+"",mActivity)));
        holder.item_thickness.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_thickness)+" ",mActivity),"\n",StaticFunctions.setSpannableText(String.valueOf(mlist.get(position).getThickness()),mActivity)));
        holder.item_width.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_width)+" ",mActivity),"\n",StaticFunctions.setSpannableText(String.valueOf(mlist.get(position).getWidth()),mActivity)));
        holder.item_length.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_length)+" ",mActivity),"\n",StaticFunctions.setSpannableText(String.valueOf(mlist.get(position).getLength()),mActivity)));
        holder.item_stock.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_stock)+" ",mActivity),"\n",StaticFunctions.setSpannableText(mlist.get(position).getStock(),mActivity)));

    }





    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView item_stock,item_thickness,item_width,item_length, total_items;
        LinearLayout main_container;

        public MyViewHolder(View view) {
            super(view);
            total_items = view.findViewById(R.id.total_items);
            item_stock = view.findViewById(R.id.item_stock);
            main_container = (LinearLayout) view.findViewById(R.id.main_container);
            item_thickness = view.findViewById(R.id.item_thickness);
            item_width = view.findViewById(R.id.item_width);
            item_length = view.findViewById(R.id.item_length);

        }
    }


}
