package com.abdeveloper.maniartradelink.home.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.adapter.ItemListAdapter;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.home.model.LeadModel;
import com.abdeveloper.maniartradelink.meeting.BottomSheetMaps;
import com.abdeveloper.maniartradelink.meeting.model.MapDetailsModel;
import com.abdeveloper.maniartradelink.orders.model.UnitCodeModel;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 5/2/19.
 */

public class FragmentLeads extends Fragment {

    @BindView(R.id.company_name)
    MaterialEditText company_name;

    @BindView(R.id.company_address)
    MaterialEditText company_address;

    @BindView(R.id.delegation_person_name)
    MaterialEditText delegation_person_name;

    @BindView(R.id.phone_number)
    MaterialEditText phone_number;

    @BindView(R.id.company_email)
    MaterialEditText company_email;

    @BindView(R.id.add_lead)
    AppCompatButton add_lead;

    @BindView(R.id.fetch_location)
    ImageView fetch_location;

   @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    Double latitude=null;
    Double longitude=null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_leads_layout, container, false);

        ButterKnife.bind(this, view);

        fetch_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetMaps bottomSheetMaps = new BottomSheetMaps();
                bottomSheetMaps.setLocationSelectedListener(new BottomSheetMaps.LocationSelectedListener() {
                    @Override
                    public void onLocationSelected(MapDetailsModel mapDetailsModel) {
                        latitude = mapDetailsModel.getLatitude();
                        longitude = mapDetailsModel.getLongitude();

                        if(mapDetailsModel.getName()!=null){
                            company_name.setText(mapDetailsModel.getName());
                        }

                        if(mapDetailsModel.getAddress()!=null){
                            company_address.setText(mapDetailsModel.getAddress());
                        }
                    }
                });
                //show it
                bottomSheetMaps.show(getActivity().getSupportFragmentManager(), Constants.ADD_ITEM_DIALOG);

            }
        });


        add_lead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    LeadModel model = new LeadModel();
                    model.setCompany_name(company_name.getText().toString());
                    model.setDelegation_person(delegation_person_name.getText().toString());
                    model.setCompany_address(company_address.getText().toString());
                    model.setPhone_number(phone_number.getText().toString());

                    if(!company_email.getText().toString().equals("")){
                        model.setEmail_id(company_email.getText().toString());
                    }

                    if(latitude!=null) {
                        model.setAddress_latitude(latitude);
                    }

                    if(longitude!=null) {
                        model.setAddress_longitude(longitude);
                    }
                    progress_bar.setVisibility(View.VISIBLE);
                    new ServiceAPI(getActivity()).postLeads(model).enqueue(new Callback<LeadModel>() {
                        @Override
                        public void onResponse(Call<LeadModel> call, Response<LeadModel> response) {
                            progress_bar.setVisibility(View.GONE);
                            if(response.isSuccessful() && isAdded()){
                                Toast.makeText(getActivity(),"Lead added successfully",Toast.LENGTH_LONG).show();
                                getActivity().finish();
                            }else{
                                if(isAdded())
                                    StaticFunctions.commonError(response, getActivity());
                            }
                        }

                        @Override
                        public void onFailure(Call<LeadModel> call, Throwable t) {
                            progress_bar.setVisibility(View.GONE);
                            StaticFunctions.showAllFailureError(t,getActivity());
                        }
                    });
                }
            }
        });

        return view;

    }


    private boolean validate() {
        if(company_name.getText().toString().equals("")){
            Toast.makeText(getContext(),"Company name cannot be empty",Toast.LENGTH_LONG).show();
            return false;
        }

        if(company_address.getText().toString().equals("")){
            Toast.makeText(getContext(),"Company address cannot be empty",Toast.LENGTH_LONG).show();
            return false;
        }

        if(delegation_person_name.getText().toString().equals("")){
            Toast.makeText(getContext(),"Delegation person name cannot be empty",Toast.LENGTH_LONG).show();
            return false;
        }

        if(phone_number.getText().toString().equals("")){
            Toast.makeText(getContext(),"Phone number cannot be empty",Toast.LENGTH_LONG).show();
            return false;
        }

        return true;

    }

}
