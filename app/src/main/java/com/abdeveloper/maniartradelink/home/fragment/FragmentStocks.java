package com.abdeveloper.maniartradelink.home.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.adapter.ItemListAdapter;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 5/2/19.
 */

public class FragmentStocks extends Fragment {

    @BindView(R.id.stock_list_recyclerview)
    RecyclerView stock_list_recyclerview;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    ArrayList<ItemModel> itemModels = new ArrayList<>();

    ItemListAdapter itemListAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stock_layout, container, false);

        ButterKnife.bind(this,view);


        setUpList();

        getDataFromServer();


        return view;

    }

    private void getDataFromServer() {

        itemModels.clear();

        //Call server
        new ServiceAPI(getContext()).getItems().enqueue(new Callback<ArrayList<ItemModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ItemModel>> call, Response<ArrayList<ItemModel>> response) {
                progress_bar.setVisibility(View.GONE);
                if(response.isSuccessful() && isAdded()){
                    itemModels.addAll(response.body());
                    itemListAdapter = new ItemListAdapter((AppCompatActivity) getActivity(),itemModels);
                    stock_list_recyclerview.setAdapter(itemListAdapter);
                }else{
                    if(isAdded())
                        StaticFunctions.commonError(response, getActivity());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ItemModel>> call, Throwable t) {
                progress_bar.setVisibility(View.GONE);
                StaticFunctions.showAllFailureError(t,getActivity());

            }
        });


    }

    private void setUpList() {

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        stock_list_recyclerview.setLayoutManager(mLayoutManager);

    }
}
