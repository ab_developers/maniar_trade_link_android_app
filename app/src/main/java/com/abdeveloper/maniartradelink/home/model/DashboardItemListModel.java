package com.abdeveloper.maniartradelink.home.model;

import android.graphics.drawable.Drawable;

/**
 * Created by root on 5/2/19.
 */

public class DashboardItemListModel {
    String title;
    Drawable image;

    public DashboardItemListModel(String title, Drawable image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }
}
