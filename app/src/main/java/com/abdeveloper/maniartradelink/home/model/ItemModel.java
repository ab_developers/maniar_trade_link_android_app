package com.abdeveloper.maniartradelink.home.model;

/**
 * Created by root on 5/2/19.
 */

public class ItemModel {

    String id;
    String name;
    Integer length;
    Integer width;
    Double thickness;
    String category;
    String stock;

    public ItemModel(String name, Integer length, Integer width, Double thickness, String category, String stock) {
        this.name = name;
        this.length = length;
        this.width = width;
        this.thickness = thickness;
        this.category = category;
        this.stock = stock;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Double getThickness() {
        return thickness;
    }

    public void setThickness(Double thickness) {
        this.thickness = thickness;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
}
