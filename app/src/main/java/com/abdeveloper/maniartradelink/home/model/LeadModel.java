package com.abdeveloper.maniartradelink.home.model;

/**
 * Created by root on 18/2/19.
 */

public class LeadModel {

    String company_name;
    String company_address;
    String delegation_person;
    String phone_number;
    Double address_latitude;
    Double address_longitude;
    String email_id;


    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getDelegation_person() {
        return delegation_person;
    }

    public void setDelegation_person(String delegation_person) {
        this.delegation_person = delegation_person;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public Double getAddress_latitude() {
        return address_latitude;
    }

    public void setAddress_latitude(Double address_latitude) {
        this.address_latitude = address_latitude;
    }

    public Double getAddress_longitude() {
        return address_longitude;
    }

    public void setAddress_longitude(Double address_longitude) {
        this.address_longitude = address_longitude;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }
}
