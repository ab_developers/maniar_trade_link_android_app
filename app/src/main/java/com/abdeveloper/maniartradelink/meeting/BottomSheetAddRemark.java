package com.abdeveloper.maniartradelink.meeting;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.orders.adapter.SpinnerAdapterItems;
import com.abdeveloper.maniartradelink.orders.adapter.SpinnerAdapterUnitCode;
import com.abdeveloper.maniartradelink.orders.model.ItemDetailModel;
import com.abdeveloper.maniartradelink.orders.model.LineItemModel;
import com.abdeveloper.maniartradelink.orders.model.UnitCodeModel;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 6/2/19.
 */

public class BottomSheetAddRemark extends BottomSheetDialogFragment {


    public interface AddRemarkListener {
        public void onRemarkAdded(String remark);
    }


    AddRemarkListener addRemarkListener;

    public void setAddRemarkListener(AddRemarkListener addRemarkListener) {
        this.addRemarkListener = addRemarkListener;
    }

    Dialog dialog;

    @BindView(R.id.remark)
    MaterialEditText remark;

    @BindView(R.id.add_remark)
    AppCompatButton add_remark;

    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();
        //this lines are necassary for DIALOG TO OPEN FULL

    }

    private void initView(View contentView) {

        ButterKnife.bind(this, contentView);

        if(getArguments()!=null){
            if(!getArguments().getString("status","").equals("")){
                String status = getArguments().getString("status","");
                if(status.equals(Constants.MEETING_CANCELLED)){
                    add_remark.setText(R.string.cancel_meeting_btn_text);
                    add_remark.setAllCaps(true);
                }
                if(status.equals(Constants.MEETING_COMPLETED)){
                    add_remark.setText(R.string.end_meeting_btn_text);
                    add_remark.setAllCaps(true);

                }
            }
        }


        add_remark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!remark.getText().toString().equals("")){
                    addRemarkListener.onRemarkAdded(remark.getText().toString());
                    dismiss();
                }else{
                    Toast.makeText(getActivity(),"Please add the remark",Toast.LENGTH_LONG).show();
                }
            }
        });

    }





    @Override
    public void setupDialog(Dialog dialog, int style) {
        // super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.custom_add_remark_bottom_sheet, null);
        dialog.setContentView(contentView);
        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        initView(contentView);
    }


    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onResume() {
        super.onResume();
    }


}


