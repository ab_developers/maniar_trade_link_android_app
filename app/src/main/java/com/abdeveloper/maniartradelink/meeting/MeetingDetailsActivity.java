package com.abdeveloper.maniartradelink.meeting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.abdeveloper.maniartradelink.BaseActivity;
import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.meeting.adapter.MeetingListAdapter;
import com.abdeveloper.maniartradelink.meeting.model.MeetingModel;
import com.abdeveloper.maniartradelink.orders.OrderDetailsActivity;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 23/2/19.
 */

public class MeetingDetailsActivity extends BaseActivity {

    public Integer REQUEST_CODE_START_MEETING = 0;
    public Integer REQUEST_CODE_END_MEETING = 1;

    @BindView(R.id.company_name)
    TextView company_name;

    @BindView(R.id.meeting_time)
    TextView meeting_time;

    @BindView(R.id.delegation_person_name)
    TextView delegation_person_name;

    @BindView(R.id.phone_number)
    TextView phone_number;

    @BindView(R.id.meeting_address)
    TextView meeting_address;

    @BindView(R.id.meeting_purpose)
    TextView meeting_purpose;

    @BindView(R.id.meeting_remark)
    TextView meeting_remark;

    @BindView(R.id.status)
    TextView meeting_status_textview;

    @BindView(R.id.status_container)
    CardView status_container;

    @BindView(R.id.btn_show_meeting_location)
    AppCompatButton btn_show_meeting_location;

    @BindView(R.id.btn_start_meeting)
    AppCompatButton btn_start_meeting;

    @BindView(R.id.btn_cancel_meeting)
    AppCompatButton btn_cancel_meeting;

    @BindView(R.id.btn_end_meeting)
    AppCompatButton btn_end_meeting;

    @BindView(R.id.main_progress_bar)
    ProgressBar progress_bar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

   @BindView(R.id.meeting_status_action_layout)
   CardView meeting_status_action_layout;

    String MeetingID = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meeting_details_activity_layout);

        ButterKnife.bind(this);

        toolbar.setTitle("Meeting Details");
        toolbar.setTitleTextColor(ContextCompat.getColor(MeetingDetailsActivity.this, R.color.white));


        getIntentFromPrevious();

        getDataFromServer();

        buttonActions();

    }

    private void buttonActions() {

        btn_show_meeting_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn_cancel_meeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Take remark
                BottomSheetAddRemark bottomSheetAddRemark = new BottomSheetAddRemark();
                Bundle bundle = new Bundle();
                bundle.putString("status",Constants.MEETING_CANCELLED);
                bottomSheetAddRemark.setArguments(bundle);
                bottomSheetAddRemark.show(getSupportFragmentManager(), Constants.BOTTOM_SHEET_ADD_REMARK);
                bottomSheetAddRemark.setAddRemarkListener(new BottomSheetAddRemark.AddRemarkListener() {
                    @Override
                    public void onRemarkAdded(String remark) {
                        MeetingModel meetingModel = new MeetingModel();
                        meetingModel.setRemark(remark);
                        meetingModel.setStatus(Constants.CANCELLED);

                        updateMeeting(meetingModel);
                    }
                });




            }
        });

        btn_end_meeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MeetingDetailsActivity.this, CallbackLocationFetchingActivity.class);
                startActivityForResult(intent, REQUEST_CODE_END_MEETING);
            }
        });


        btn_start_meeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MeetingDetailsActivity.this, CallbackLocationFetchingActivity.class);
                startActivityForResult(intent, REQUEST_CODE_START_MEETING);
            }
        });

    }

    public void updateMeeting(int STATUS,Double latitude,Double longitude,String remark){
        switch (STATUS){
            case 0:
                MeetingModel meetingModel = new MeetingModel();
                meetingModel.setStart_latitude(latitude);
                meetingModel.setStart_longitude(longitude);
                meetingModel.setStatus(Constants.MEETING_STARTED);
                updateMeeting(meetingModel);
                break;
            case 1:
                MeetingModel meetingModelEnd = new MeetingModel();
                meetingModelEnd.setRemark(remark);
                meetingModelEnd.setEnd_latitude(latitude);
                meetingModelEnd.setEnd_longitude(longitude);
                meetingModelEnd.setStatus(Constants.MEETING_COMPLETED);
                updateMeeting(meetingModelEnd);
                break;
        }
    }

    private void updateMeeting(MeetingModel meetingModel) {
        progress_bar.setVisibility(View.VISIBLE);
        new ServiceAPI(MeetingDetailsActivity.this).patchMeetingDetails(MeetingID,meetingModel).enqueue(new Callback<MeetingModel>() {
            @Override
            public void onResponse(Call<MeetingModel> call, Response<MeetingModel> response) {
                progress_bar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    configuringResponse(response);
                } else {
                    StaticFunctions.commonError(response, MeetingDetailsActivity.this);
                }
            }

            @Override
            public void onFailure(Call<MeetingModel> call, Throwable t) {
                progress_bar.setVisibility(View.GONE);
                StaticFunctions.showAllFailureError(t, MeetingDetailsActivity.this);

            }
        });
    }

    private void getDataFromServer() {
        new ServiceAPI(MeetingDetailsActivity.this).getMeetingDetails(MeetingID).enqueue(new Callback<MeetingModel>() {
            @Override
            public void onResponse(Call<MeetingModel> call, Response<MeetingModel> response) {
                progress_bar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    configuringResponse(response);
                } else {
                    StaticFunctions.commonError(response, MeetingDetailsActivity.this);
                }
            }

            @Override
            public void onFailure(Call<MeetingModel> call, Throwable t) {
                progress_bar.setVisibility(View.GONE);
                StaticFunctions.showAllFailureError(t, MeetingDetailsActivity.this);

            }
        });
    }

    private void configuringResponse(Response<MeetingModel> response) {

        company_name.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(MeetingDetailsActivity.this.getString(R.string.company_name)+" ",MeetingDetailsActivity.this),"\n",StaticFunctions.setSpannableText(response.body().getCompany_name(),MeetingDetailsActivity.this)));
        meeting_time.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(MeetingDetailsActivity.this.getString(R.string.meeting_time)+" ",MeetingDetailsActivity.this),"\n",StaticFunctions.setSpannableText(StaticFunctions.currentIST((response.body().getMeeting_time())),MeetingDetailsActivity.this)));
        delegation_person_name.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(MeetingDetailsActivity.this.getString(R.string.delegation_person_name)+" ",MeetingDetailsActivity.this),"\n",StaticFunctions.setSpannableText(response.body().getDelegation_person(),MeetingDetailsActivity.this)));
        phone_number.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(MeetingDetailsActivity.this.getString(R.string.phone_number)+" ",MeetingDetailsActivity.this),"\n",StaticFunctions.setSpannableText(response.body().getPhone_number(),MeetingDetailsActivity.this)));
        meeting_address.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(MeetingDetailsActivity.this.getString(R.string.company_address)+" ",MeetingDetailsActivity.this),"\n",StaticFunctions.setSpannableText(response.body().getCompany_address(),MeetingDetailsActivity.this)));

        meeting_purpose.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(MeetingDetailsActivity.this.getString(R.string.meeting_purpose)+" ",MeetingDetailsActivity.this),"\n",StaticFunctions.setSpannableText(response.body().getPurpose(),MeetingDetailsActivity.this)));
        if(response.body().getRemark()!=null){
            meeting_remark.setVisibility(View.VISIBLE);
            meeting_remark.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(MeetingDetailsActivity.this.getString(R.string.meeting_remark)+" ",MeetingDetailsActivity.this),"\n",StaticFunctions.setSpannableText(response.body().getRemark(),MeetingDetailsActivity.this)));


        }else{
            meeting_remark.setVisibility(View.GONE);
        }


        setStatusAction(response.body().getStatus());


    }

    private void setStatusAction(String status) {
        status_container.setVisibility(View.VISIBLE);
        if (!status.equals(Constants.CANCELLED) && !status.equals(Constants.MEETING_COMPLETED)) {
            meeting_status_action_layout.setVisibility(View.VISIBLE);
            switch (status) {
                case Constants.PENDING:
                    btn_start_meeting.setVisibility(View.VISIBLE);
                    btn_cancel_meeting.setVisibility(View.VISIBLE);
                    btn_end_meeting.setVisibility(View.GONE);

                    meeting_status_textview.setText(status.toUpperCase());
                    meeting_status_textview.setTextColor(ContextCompat.getColor(MeetingDetailsActivity.this, R.color.colorRed));

                    break;
                case Constants.MEETING_STARTED:
                    btn_start_meeting.setVisibility(View.GONE);
                    btn_cancel_meeting.setVisibility(View.VISIBLE);
                    btn_end_meeting.setVisibility(View.VISIBLE);

                    meeting_status_textview.setText(status.toUpperCase());
                    meeting_status_textview.setTextColor(ContextCompat.getColor(MeetingDetailsActivity.this, R.color.green));


                    break;
            }
        } else {
            meeting_status_action_layout.setVisibility(View.GONE);

            switch (status) {
                case Constants.CANCELLED:
                    meeting_status_textview.setText(status.toUpperCase());
                    meeting_status_textview.setTextColor(ContextCompat.getColor(MeetingDetailsActivity.this, R.color.colorRed));

                    break;
                case Constants.MEETING_COMPLETED:
                    meeting_status_textview.setText(status.toUpperCase());
                    meeting_status_textview.setTextColor(ContextCompat.getColor(MeetingDetailsActivity.this, R.color.green));
                    break;
            }


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_END_MEETING && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Bundle bundle1 = data.getExtras();
                final Double latitude = bundle1.getDouble("latitude");
                final Double longitude =bundle1.getDouble("longitude");

                BottomSheetAddRemark bottomSheetAddRemark = new BottomSheetAddRemark();
                Bundle bundle = new Bundle();
                bundle.putString("status",Constants.MEETING_COMPLETED);
                bottomSheetAddRemark.setArguments(bundle);
                bottomSheetAddRemark.show(getSupportFragmentManager(), Constants.BOTTOM_SHEET_ADD_REMARK);
                bottomSheetAddRemark.setAddRemarkListener(new BottomSheetAddRemark.AddRemarkListener() {
                    @Override
                    public void onRemarkAdded(String remark) {
                        //Patching Data
                        updateMeeting(REQUEST_CODE_END_MEETING,latitude,longitude,remark);
                    }
                });



            }
        }else if(requestCode == REQUEST_CODE_START_MEETING && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Bundle bundle1 = data.getExtras();
                Double latitude = bundle1.getDouble("latitude");
                Double longitude =bundle1.getDouble("longitude");

                //Patching Data
                updateMeeting(REQUEST_CODE_START_MEETING,latitude,longitude,null);
            }
        }
    }

    private void getIntentFromPrevious() {
        MeetingID = getIntent().getStringExtra(Constants.MEETING_ID);
    }
}
