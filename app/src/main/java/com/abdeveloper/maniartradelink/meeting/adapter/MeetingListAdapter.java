package com.abdeveloper.maniartradelink.meeting.adapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.meeting.MeetingDetailsActivity;
import com.abdeveloper.maniartradelink.meeting.model.MeetingModel;
import com.abdeveloper.maniartradelink.orders.OrderDetailsActivity;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;

import java.util.ArrayList;

/**
 * Created by root on 5/2/19.
 */

public class MeetingListAdapter extends RecyclerView.Adapter<MeetingListAdapter.MyViewHolder> {

    private AppCompatActivity mActivity;
    private ArrayList<MeetingModel> mlist;

    public MeetingListAdapter(AppCompatActivity mActivity, ArrayList<MeetingModel> cartModelArrayList) {
        this.mActivity = mActivity;
        this.mlist = cartModelArrayList;
    }

    @Override
    public MeetingListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.meeting_list_item_layout, parent, false);
        return new MeetingListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MeetingListAdapter.MyViewHolder holder, final int position) {

        holder.company_name.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.company_name)+" ",mActivity),"\n",StaticFunctions.setSpannableText(mlist.get(position).getCompany_name(),mActivity)));
        holder.meeting_time.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.meeting_time)+" ",mActivity),"\n",StaticFunctions.setSpannableText(StaticFunctions.currentIST((mlist.get(position).getMeeting_time())),mActivity)));
        holder.delegation_person_name.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.delegation_person_name)+" ",mActivity),"\n",StaticFunctions.setSpannableText(mlist.get(position).getDelegation_person(),mActivity)));
        holder.phone_number.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.phone_number)+" ",mActivity),"\n",StaticFunctions.setSpannableText(mlist.get(position).getPhone_number(),mActivity)));
        holder.meeting_purpose.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.meeting_purpose)+" ",mActivity),"\n",StaticFunctions.setSpannableText(mlist.get(position).getPurpose(),mActivity)));
        holder.meeting_status.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_status)+" ",mActivity),"\n",StaticFunctions.setSpannableText(mlist.get(position).getStatus().toUpperCase(),mActivity)));
        holder.meeting_address.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.company_address)+" ",mActivity),"\n",StaticFunctions.setSpannableText(mlist.get(position).getCompany_address(),mActivity)));

        holder.main_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, MeetingDetailsActivity.class);
                intent.putExtra(Constants.MEETING_ID, mlist.get(position).getId());
                mActivity.startActivity(intent);
            }
        });
    }





    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView company_name,meeting_time,delegation_person_name,phone_number, meeting_purpose,meeting_status,meeting_address;
        LinearLayout main_container;

        public MyViewHolder(View view) {
            super(view);
            company_name = view.findViewById(R.id.company_name);
            meeting_time = view.findViewById(R.id.meeting_time);
            main_container = (LinearLayout) view.findViewById(R.id.main_container);
            delegation_person_name = view.findViewById(R.id.delegation_person_name);
            phone_number = view.findViewById(R.id.phone_number);
            meeting_purpose = view.findViewById(R.id.meeting_purpose);
            meeting_status = view.findViewById(R.id.meeting_status);
            meeting_address = view.findViewById(R.id.meeting_address);

        }
    }


}
