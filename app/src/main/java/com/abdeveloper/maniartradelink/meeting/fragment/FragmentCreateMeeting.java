package com.abdeveloper.maniartradelink.meeting.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.meeting.BottomSheetMaps;
import com.abdeveloper.maniartradelink.meeting.CallbackLocationFetchingActivity;
import com.abdeveloper.maniartradelink.meeting.model.MapDetailsModel;
import com.abdeveloper.maniartradelink.meeting.model.MeetingModel;
import com.abdeveloper.maniartradelink.orders.BottomSheetAddItem;
import com.abdeveloper.maniartradelink.orders.model.LineItemModel;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 5/2/19.
 */

public class FragmentCreateMeeting extends Fragment {

    @BindView(R.id.company_name)
    MaterialEditText company_name;

    @BindView(R.id.company_address)
    MaterialEditText company_address;

    @BindView(R.id.delegation_person_name)
    MaterialEditText delegation_person_name;

    @BindView(R.id.phone_number)
    MaterialEditText phone_number;

    @BindView(R.id.meeting_purpose)
    MaterialEditText meeting_purpose;

    @BindView(R.id.meeting_date_time)
    TextView meeting_date_time;

    @BindView(R.id.meeting_time_card)
    CardView meeting_time_card;

    @BindView(R.id.add_meeting)
    AppCompatButton add_meeting;

    @BindView(R.id.fetch_location)
    ImageView fetch_location;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    DatePickerDialog.OnDateSetListener dueDateListener;
    TimePickerDialog.OnTimeSetListener timeSetListener;

    Calendar myCalendar;

    Double latitude = null;
    Double longitude = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meeting_layout, container, false);

        ButterKnife.bind(this, view);

        myCalendar = Calendar.getInstance();
        updateLabelDate(myCalendar);

        fetch_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetMaps bottomSheetMaps = new BottomSheetMaps();
                bottomSheetMaps.setLocationSelectedListener(new BottomSheetMaps.LocationSelectedListener() {
                    @Override
                    public void onLocationSelected(MapDetailsModel mapDetailsModel) {
                        latitude = mapDetailsModel.getLatitude();
                        longitude = mapDetailsModel.getLongitude();

                        if(mapDetailsModel.getName()!=null){
                            company_name.setText(mapDetailsModel.getName());
                        }

                        if(mapDetailsModel.getAddress()!=null){
                            company_address.setText(mapDetailsModel.getAddress());
                        }
                    }
                });
                //show it
                bottomSheetMaps.show(getActivity().getSupportFragmentManager(), Constants.ADD_ITEM_DIALOG);

            }
        });

        meeting_time_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                DatePickerDialog dialog = new DatePickerDialog(getActivity(),R.style.DatePickerTheme, dueDateListener, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                dialog.getDatePicker().setMinDate(System.currentTimeMillis());
                dialog.show();
            }
        });

        dueDateListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
                int minute = myCalendar.get(Calendar.MINUTE);


                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), timeSetListener, hour, minute, false);
                timePickerDialog.show();
            }

        };


        timeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {

                myCalendar.set(Calendar.HOUR_OF_DAY, i);
                myCalendar.set(Calendar.MINUTE, i1);

                updateLabelDate(myCalendar);
            }
        };



        add_meeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    MeetingModel model = new MeetingModel();
                    model.setCompany_name(company_name.getText().toString());
                    model.setDelegation_person(delegation_person_name.getText().toString());
                    model.setCompany_address(company_address.getText().toString());
                    model.setPhone_number(phone_number.getText().toString());
                    model.setPurpose(meeting_purpose.getText().toString());
                    model.setMeeting_time(StaticFunctions.currentUTC(meeting_date_time.getText().toString()));

                    if(latitude!=null) {
                        model.setAddress_latitude(latitude);
                    }

                    if(longitude!=null) {
                        model.setAddress_longitude(longitude);
                    }




                    progress_bar.setVisibility(View.VISIBLE);
                    new ServiceAPI(getActivity()).postMeetings(model).enqueue(new Callback<MeetingModel>() {
                        @Override
                        public void onResponse(Call<MeetingModel> call, Response<MeetingModel> response) {
                            progress_bar.setVisibility(View.GONE);
                            if (response.isSuccessful() && isAdded()) {
                                Toast.makeText(getActivity(), "Meeting created successfully", Toast.LENGTH_LONG).show();
                                getActivity().finish();
                            } else {
                                if (isAdded())
                                    StaticFunctions.commonError(response, getActivity());
                            }
                        }

                        @Override
                        public void onFailure(Call<MeetingModel> call, Throwable t) {
                            progress_bar.setVisibility(View.GONE);
                            StaticFunctions.showAllFailureError(t, getActivity());
                        }
                    });
                }
            }
        });

        return view;

    }


    private boolean validate() {
        if (company_name.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Company name cannot be empty", Toast.LENGTH_LONG).show();
            return false;
        }

        if (company_address.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Company address cannot be empty", Toast.LENGTH_LONG).show();
            return false;
        }

        if (delegation_person_name.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Delegation person name cannot be empty", Toast.LENGTH_LONG).show();
            return false;
        }

        if (phone_number.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Phone number cannot be empty", Toast.LENGTH_LONG).show();
            return false;
        }

        if (meeting_purpose.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Meeting purpose cannot be empty", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;

    }

    private void updateLabelDate(Calendar myCalendar) {//In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.US);
        if (checkIfDateIsAfterCurrentDate(sdf.format(myCalendar.getTime()))) {
            Toast.makeText(getContext(), getString(R.string.select_future_date), Toast.LENGTH_LONG).show();
        } else {
            meeting_date_time.setText(sdf.format(myCalendar.getTime()));
        }
    }

    public Boolean checkIfDateIsAfterCurrentDate(String due_date) {

        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.US);
        Date dueDate, currentDate;
        try {
            dueDate = format.parse(due_date);


            //checking
            Calendar calendar = Calendar.getInstance();
            String dateOutput = format.format(calendar.getTime());
            currentDate = format.parse(dateOutput);

            //Toast.makeText(TempMainActivity.this,dateOutput+"end date:"+subscriptionEndDate,Toast.LENGTH_SHORT).show();
            if (currentDate.after(dueDate)) {
                return true;
            }


        } catch (Exception e) {
            return false;
        }

        return false;
    }


}
