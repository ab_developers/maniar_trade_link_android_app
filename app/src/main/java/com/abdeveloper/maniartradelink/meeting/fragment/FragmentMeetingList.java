package com.abdeveloper.maniartradelink.meeting.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.abdeveloper.maniartradelink.ApplicationClass;
import com.abdeveloper.maniartradelink.OpenContainer;
import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.adapter.ItemListAdapter;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.meeting.adapter.MeetingListAdapter;
import com.abdeveloper.maniartradelink.meeting.model.MeetingModel;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 5/2/19.
 */

public class FragmentMeetingList extends Fragment {

    @BindView(R.id.meeting_list_recyclerview)
    RecyclerView meeting_list_recyclerview;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    @BindView(R.id.create_meeting)
    FloatingActionButton create_meeting;

    ArrayList<MeetingModel> meetingModels = new ArrayList<>();

    MeetingListAdapter meetingListAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meetings_layout, container, false);

        ButterKnife.bind(this,view);


        setUpList();

        getDataFromServer();


        create_meeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationClass.CONTAINERFRAG = new FragmentCreateMeeting();
                ApplicationClass.CONTAINER_TITLE = getActivity().getString(R.string.create_meeting_fragment);
                ApplicationClass.CONTAINERFRAGTAG = Constants.FRAGMENT_MEETING_CREATE_TAG;


                Intent intent3 = new Intent(getActivity(), OpenContainer.class);
                intent3.putExtra("toolbarCategory", OpenContainer.COMMON_MENU);
                getActivity().startActivity(intent3);
            }
        });

        return view;

    }

    private void getDataFromServer() {

        meetingModels.clear();

        //Call server
        new ServiceAPI(getContext()).getMeetings().enqueue(new Callback<ArrayList<MeetingModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MeetingModel>> call, Response<ArrayList<MeetingModel>> response) {
                progress_bar.setVisibility(View.GONE);
                if(response.isSuccessful() && isAdded()){
                    meetingModels.addAll(response.body());
                    meetingListAdapter = new MeetingListAdapter((AppCompatActivity) getActivity(),meetingModels);
                    meeting_list_recyclerview.setAdapter(meetingListAdapter);
                }else{
                    if(isAdded())
                        StaticFunctions.commonError(response, getActivity());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MeetingModel>> call, Throwable t) {
                progress_bar.setVisibility(View.GONE);
                StaticFunctions.showAllFailureError(t,getActivity());

            }
        });


    }

    private void setUpList() {

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        meeting_list_recyclerview.setLayoutManager(mLayoutManager);

    }
}
