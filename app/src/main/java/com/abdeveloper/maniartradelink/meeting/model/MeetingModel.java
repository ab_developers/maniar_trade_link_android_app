package com.abdeveloper.maniartradelink.meeting.model;

/**
 * Created by root on 19/2/19.
 */

public class MeetingModel {

    String id;
    String meeting_time;
    String company_name;
    String company_address;
    String delegation_person;
    String phone_number;
    Double start_latitude;
    Double start_longitude;
    Double address_latitude;
    Double address_longitude;
    Double end_latitude;
    Double end_longitude;
    String purpose;
    String status;
    String remark;

    public Double getAddress_latitude() {
        return address_latitude;
    }

    public void setAddress_latitude(Double address_latitude) {
        this.address_latitude = address_latitude;
    }

    public Double getAddress_longitude() {
        return address_longitude;
    }

    public void setAddress_longitude(Double address_longitude) {
        this.address_longitude = address_longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMeeting_time() {
        return meeting_time;
    }

    public void setMeeting_time(String meeting_time) {
        this.meeting_time = meeting_time;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getDelegation_person() {
        return delegation_person;
    }

    public void setDelegation_person(String delegation_person) {
        this.delegation_person = delegation_person;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public Double getStart_latitude() {
        return start_latitude;
    }

    public void setStart_latitude(Double start_latitude) {
        this.start_latitude = start_latitude;
    }

    public Double getStart_longitude() {
        return start_longitude;
    }

    public void setStart_longitude(Double start_longitude) {
        this.start_longitude = start_longitude;
    }

    public Double getEnd_latitude() {
        return end_latitude;
    }

    public void setEnd_latitude(Double end_latitude) {
        this.end_latitude = end_latitude;
    }

    public Double getEnd_longitude() {
        return end_longitude;
    }

    public void setEnd_longitude(Double end_longitude) {
        this.end_longitude = end_longitude;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
