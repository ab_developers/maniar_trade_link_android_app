package com.abdeveloper.maniartradelink.orders;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.adapter.ItemListAdapter;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.orders.adapter.SpinnerAdapterItems;
import com.abdeveloper.maniartradelink.orders.adapter.SpinnerAdapterUnitCode;
import com.abdeveloper.maniartradelink.orders.model.ItemDetailModel;
import com.abdeveloper.maniartradelink.orders.model.LineItemModel;
import com.abdeveloper.maniartradelink.orders.model.UnitCodeModel;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 6/2/19.
 */

public class BottomSheetAddItem extends BottomSheetDialogFragment {


    ArrayList<ItemModel> itemModels = new ArrayList<>();
    ArrayList<UnitCodeModel> unitCodeModels = new ArrayList<>();

    public interface AddItemListener {
        public void onItemAdded(LineItemModel lineItemModel);
    }


    AddItemListener addItemListener;

    public void setAddItemListener(AddItemListener addItemListener) {
        this.addItemListener = addItemListener;
    }

    @BindView(R.id.add_item)
    AppCompatButton add_item;

    @BindView(R.id.item_spinner)
    AppCompatSpinner item_spinner;

    @BindView(R.id.unit_spinner)
    AppCompatSpinner unit_spinner;

    @BindView(R.id.item_qty)
    MaterialEditText item_qty;

    @BindView(R.id.unit_rate)
    MaterialEditText unit_rate;

    @BindView(R.id.item_description)
    MaterialEditText item_description;

    @BindView(R.id.item_card)
    CardView item_card;


    SpinnerAdapterItems spinnerAdapterItems;
    SpinnerAdapterUnitCode spinnerAdapterUnitCode;

    Dialog dialog;

    String item_id_text = "";
    String item_unit_code_text= "";


    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();
        //this lines are necassary for DIALOG TO OPEN FULL

    }

    private void initView(View contentView) {

        ButterKnife.bind(this, contentView);

        //Check if update mode or Add mode
        checkIfAddOrUpdate();

        getItems();
        getUnitCode();



        add_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //LineItemModel lineItemModel = new LineItemModel();
                //addItemListener.onItemAdded(lineItemModel);
                // )

                if(validate()){
                    LineItemModel lineItemModel = new LineItemModel();

                    //Setting item
                    ItemDetailModel itemDetailModel = new ItemDetailModel();
                    itemDetailModel.setName(((ItemModel) item_spinner.getSelectedItem()).getName());
                    itemDetailModel.setId(((ItemModel) item_spinner.getSelectedItem()).getId());
                    lineItemModel.setItem(itemDetailModel);

                    //Setting other values
                    lineItemModel.setUnit_code(((UnitCodeModel) unit_spinner.getSelectedItem()).getName());
                    lineItemModel.setQuantity(Integer.valueOf(item_qty.getText().toString()));
                    lineItemModel.setUnit_rate(Double.valueOf(unit_rate.getText().toString()));
                    lineItemModel.setDescription(item_description.getText().toString());

                    addItemListener.onItemAdded(lineItemModel);

                    Toast.makeText(getContext(), ((ItemModel) item_spinner.getSelectedItem()).getName()+" added" , Toast.LENGTH_LONG).show();
                    dismiss();

                }

            }
        });

    }

    private void checkIfAddOrUpdate() {
        if(getArguments()!=null){
            String item_qty_text;
            String item_unit_rate_text;
            String item_desc_text;

            item_id_text = getArguments().getString("item_id");
            item_unit_code_text = getArguments().getString("item_unit_code");
            item_qty_text = getArguments().getString("item_qty");
            item_unit_rate_text = getArguments().getString("item_unit_rate");
            item_desc_text = getArguments().getString("item_desc");

            item_description.setText(item_desc_text);
            item_qty.setText(item_qty_text);
            unit_rate.setText(item_unit_rate_text);

        }
    }

    private boolean validate() {
        if(((ItemModel) item_spinner.getSelectedItem())==null){
            Toast.makeText(getContext(),"Please select Item",Toast.LENGTH_LONG).show();
            return false;
        }

        if(((UnitCodeModel) unit_spinner.getSelectedItem())==null){
            Toast.makeText(getContext(),"Please select unit",Toast.LENGTH_LONG).show();
            return false;
        }

        if(item_qty.getText().toString().equals("")){
            Toast.makeText(getContext(),"Please enter quantity",Toast.LENGTH_LONG).show();
            return false;
        }

        if(unit_rate.getText().toString().equals("")){
            Toast.makeText(getContext(),"Please enter unit rate for this item",Toast.LENGTH_LONG).show();
            return false;
        }

        return true;

    }

    private void getDataFromServer() {

    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        // super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.custom_add_item_bottom_sheet, null);
        dialog.setContentView(contentView);
        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        initView(contentView);
    }


    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    public void getItems() {
        itemModels.clear();

        new ServiceAPI(getContext()).getItems().enqueue(new Callback<ArrayList<ItemModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ItemModel>> call, Response<ArrayList<ItemModel>> response) {
                if (response.isSuccessful() && isAdded()) {
                    itemModels.addAll(response.body());

                    spinnerAdapterItems = new SpinnerAdapterItems(getActivity(), R.layout.spinneritem, itemModels);
                    item_spinner.setAdapter(spinnerAdapterItems);

                    //Selecting in edit mode
                    if(!item_id_text.equals("")) {
                        for (int i = 0; i < itemModels.size(); i++) {
                            if (item_id_text.equals(itemModels.get(i).getId())){
                                item_spinner.setSelection(i);

                                item_spinner.setEnabled(false);
                                item_spinner.setClickable(false);
                                break;
                            }
                        }
                    }


                } else {
                    if (isAdded())
                        StaticFunctions.commonError(response, getActivity());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ItemModel>> call, Throwable t) {
                StaticFunctions.showAllFailureError(t, getActivity());

            }
        });

    }

    public void getUnitCode() {
        unitCodeModels.clear();

        new ServiceAPI(getContext()).getUnitCode().enqueue(new Callback<ArrayList<UnitCodeModel>>() {
            @Override
            public void onResponse(Call<ArrayList<UnitCodeModel>> call, Response<ArrayList<UnitCodeModel>> response) {
                if (response.isSuccessful() && isAdded()) {
                    unitCodeModels.addAll(response.body());

                    spinnerAdapterUnitCode = new SpinnerAdapterUnitCode(getActivity(), R.layout.spinneritem, unitCodeModels);
                    unit_spinner.setAdapter(spinnerAdapterUnitCode);

                    //Selecting in edit mode
                    if(!item_unit_code_text.equals("")) {
                        for (int i = 0; i < unitCodeModels.size(); i++) {
                            if (item_unit_code_text.equals(unitCodeModels.get(i).getName())){
                                unit_spinner.setSelection(i);
                                break;
                            }
                        }
                    }

                } else {
                    if (isAdded())
                        StaticFunctions.commonError(response, getActivity());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UnitCodeModel>> call, Throwable t) {
                StaticFunctions.showAllFailureError(t, getActivity());

            }
        });

    }


}


