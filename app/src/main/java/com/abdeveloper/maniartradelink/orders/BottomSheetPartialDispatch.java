package com.abdeveloper.maniartradelink.orders;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.orders.adapter.PartialDispatchItemAdapter;
import com.abdeveloper.maniartradelink.orders.adapter.SpinnerAdapterItems;
import com.abdeveloper.maniartradelink.orders.adapter.SpinnerAdapterUnitCode;
import com.abdeveloper.maniartradelink.orders.model.ItemDetailModel;
import com.abdeveloper.maniartradelink.orders.model.LineItemModel;
import com.abdeveloper.maniartradelink.orders.model.OrderModel;
import com.abdeveloper.maniartradelink.orders.model.PostLineItemModel;
import com.abdeveloper.maniartradelink.orders.model.PostOrderModel;
import com.abdeveloper.maniartradelink.orders.model.UnitCodeModel;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 6/2/19.
 */

public class BottomSheetPartialDispatch extends BottomSheetDialogFragment {


    ArrayList<LineItemModel> postLineItemModels = new ArrayList<>();
    PartialDispatchItemAdapter partialDispatchItemAdapter;

    @BindView(R.id.partial_dispatch_list)
    RecyclerView partial_dispatch_list;

    @BindView(R.id.btn_partial_dispatch)
    AppCompatButton btn_partial_dispatch;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    @BindView(R.id.item_dispatched_completely)
    TextView item_dispatched_completely;


    public interface ItemPatchListener {
        public void onItemPatched();
    }


    ItemPatchListener addPatchListener;

    public void setItemListener(ItemPatchListener addPatchListener) {
        this.addPatchListener = addPatchListener;
    }

    Dialog dialog;

    String ORDER_ID = null;


    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();
        //this lines are necassary for DIALOG TO OPEN FULL
        //this lines are necassary for DIALOG TO OPEN FULL
        if (dialog != null) {
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    private void initView(View contentView) {

        ButterKnife.bind(this, contentView);

        setUpList();

        getDatafromIntent();

        btn_partial_dispatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<PostLineItemModel> patchLineItemModels = new ArrayList<>();
                for (int i = 0; i < postLineItemModels.size(); i++) {
                    if (postLineItemModels.get(i).getPartial_dispatched_quantity() != null && postLineItemModels.get(i).getPartial_dispatched_quantity() > 0) {
                        PostLineItemModel postLineItemModel = new PostLineItemModel();
                        postLineItemModel.setId(postLineItemModels.get(i).getId());
                        postLineItemModel.setPartial_dispatched_quantity(postLineItemModels.get(i).getPartial_dispatched_quantity());

                        Integer total_dispatch_quantity = 0;
                        if (postLineItemModels.get(i).getTotal_dispatched_quantity() != null) {
                            total_dispatch_quantity = postLineItemModels.get(i).getTotal_dispatched_quantity();
                        }
                        postLineItemModel.setTotal_dispatched_quantity(postLineItemModels.get(i).getPartial_dispatched_quantity() + total_dispatch_quantity);
                        patchLineItemModels.add(postLineItemModel);
                    }
                }

                //If no items to patch
                if (patchLineItemModels.size() < 1) {
                    Toast.makeText(getContext(), "Please enter dispatch quantity", Toast.LENGTH_LONG).show();
                    return;
                }

                PostOrderModel postOrderModel = new PostOrderModel();
                postOrderModel.setStatus(Constants.PARTIAL_DISPATCH);
                postOrderModel.setItem(patchLineItemModels);

                if (ORDER_ID == null) {
                    return;
                }

                progress_bar.setVisibility(View.VISIBLE);
                new ServiceAPI(getActivity()).patchOrderStatus(ORDER_ID, postOrderModel).enqueue(new Callback<OrderModel>() {
                    @Override
                    public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                        progress_bar.setVisibility(View.GONE);
                        if(response.isSuccessful() && isAdded()) {
                            addPatchListener.onItemPatched();
                            dismiss();
                        }else{
                            StaticFunctions.commonError(response,getActivity());
                        }
                    }

                    @Override
                    public void onFailure(Call<OrderModel> call, Throwable t) {
                        progress_bar.setVisibility(View.GONE);
                        StaticFunctions.showAllFailureError(t,getActivity());
                    }
                });
            }
        });

    }

    private void getDatafromIntent() {
        postLineItemModels = getArguments().getParcelableArrayList("partial_dispatch_item_list");
        ORDER_ID = getArguments().getString("order_id");



        //No need of partially dispatch quantity from intent as we need what we input on this page
        makeAllPartiallyDispatchedQuantityEmpty(postLineItemModels);

        //Remove item if already all quantity of that item is dispatched
        checkIfPartialItemAllQuantityDispatched(postLineItemModels);


        //Show note that all items are delivered
        if(postLineItemModels.size()<1){
            item_dispatched_completely.setVisibility(View.VISIBLE);
        }else{
            item_dispatched_completely.setVisibility(View.GONE);
        }

        partialDispatchItemAdapter = new PartialDispatchItemAdapter((AppCompatActivity) getActivity(), postLineItemModels);
        partial_dispatch_list.setAdapter(partialDispatchItemAdapter);
    }

    private void makeAllPartiallyDispatchedQuantityEmpty(ArrayList<LineItemModel> postLineItemModels) {

        for(int i=0;i<postLineItemModels.size();i++){
            postLineItemModels.get(i).setPartial_dispatched_quantity(null);
        }
    }

    private void checkIfPartialItemAllQuantityDispatched(ArrayList<LineItemModel> postLineItemModels) {

        for (Iterator<LineItemModel> iterator = postLineItemModels.iterator(); iterator.hasNext(); ) {
            LineItemModel lineItemModel = iterator.next();
            Integer total_dispatched_quantity = lineItemModel.getTotal_dispatched_quantity();

            if (total_dispatched_quantity != null) {
                if (total_dispatched_quantity.equals(lineItemModel.getQuantity())) {
                    iterator.remove();
                }
            }
        }
    }


    private void getDataFromServer() {

    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        // super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.partial_dispatch_bottom_sheet, null);
        dialog.setContentView(contentView);
        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        initView(contentView);
    }


    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    private void setUpList() {

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        partial_dispatch_list.setLayoutManager(mLayoutManager);

    }

}


