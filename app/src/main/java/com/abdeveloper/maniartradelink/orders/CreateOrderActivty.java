package com.abdeveloper.maniartradelink.orders;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.abdeveloper.maniartradelink.BaseActivity;
import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.orders.adapter.ItemDetailListAdapter;
import com.abdeveloper.maniartradelink.orders.adapter.SpinnerAdapterCompanies;
import com.abdeveloper.maniartradelink.orders.model.CompanyModel;
import com.abdeveloper.maniartradelink.orders.model.LineItemModel;
import com.abdeveloper.maniartradelink.orders.model.OrderModel;
import com.abdeveloper.maniartradelink.orders.model.PostLineItemModel;
import com.abdeveloper.maniartradelink.orders.model.PostOrderModel;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 6/2/19.
 */

public class CreateOrderActivty extends BaseActivity {


    @BindView(R.id.add_item)
    AppCompatButton add_item;

    @BindView(R.id.place_order)
    AppCompatButton place_order;

    @BindView(R.id.item_container)
    CardView item_container;

    @BindView(R.id.items_list_recyclerview)
    RecyclerView items_list_recyclerview;

    @BindView(R.id.company_spinner)
    AppCompatSpinner company_spinner;

    @BindView(R.id.tax_spinner)
    AppCompatSpinner tax_spinner;

    @BindView(R.id.order_description)
    MaterialEditText order_description;

    @BindView(R.id.perfoma)
    MaterialEditText perfoma;


    @BindView(R.id.order_remark)
    MaterialEditText order_remark;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    ArrayList<LineItemModel> lineItemModels = new ArrayList<>();

    ItemDetailListAdapter itemListAdapter;

    ArrayList<CompanyModel> companyModels = new ArrayList<>();
    SpinnerAdapterCompanies spinnerAdapterCompanies;

    Boolean isQuotation = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_order_activity_layout);

        ButterKnife.bind(this);

        toolbar.setTitle("Create Order");
        toolbar.setTitleTextColor(ContextCompat.getColor(CreateOrderActivty.this,R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(getIntent().getBooleanExtra("is_quotation",false)){
            toolbar.setTitle("Create Quotation");
            perfoma.setVisibility(View.GONE);
            isQuotation = true;
        }else{
            toolbar.setTitle("Create Order");
        }




        setUpList();

        getCompanies();


        add_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetAddItem bottomSheetDialogFragment = new BottomSheetAddItem();
                bottomSheetDialogFragment.setAddItemListener(new BottomSheetAddItem.AddItemListener() {
                    @Override
                    public void onItemAdded(LineItemModel lineItemModel) {
                        item_container.setVisibility(View.VISIBLE);
                        lineItemModels.add(lineItemModel);
                        itemListAdapter.notifyItemInserted(lineItemModels.size() - 1);

                    }
                });

                //show it
                bottomSheetDialogFragment.show(getSupportFragmentManager(), Constants.ADD_ITEM_DIALOG);
            }
        });

        place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    PostOrderModel postOrderModel = new PostOrderModel();

                    //Set Items
                    ArrayList<PostLineItemModel> postLineItemModels = new ArrayList<>();
                    for (int i = 0; i < lineItemModels.size(); i++) {
                        PostLineItemModel postLineItemModel = new PostLineItemModel();
                        postLineItemModel.setItem(lineItemModels.get(i).getItem().getId());
                        postLineItemModel.setDescription(lineItemModels.get(i).getDescription());
                        postLineItemModel.setQuantity(lineItemModels.get(i).getQuantity());
                        postLineItemModel.setUnit_code(lineItemModels.get(i).getUnit_code());
                        postLineItemModel.setUnit_rate(lineItemModels.get(i).getUnit_rate());
                        postLineItemModels.add(postLineItemModel);
                    }

                    postOrderModel.setItem(postLineItemModels);
                    postOrderModel.setCompany(((CompanyModel) company_spinner.getSelectedItem()).getId());
                    postOrderModel.setTax(tax_spinner.getSelectedItem().toString());

                    if (!perfoma.getText().toString().equals("")) {
                        postOrderModel.setDescription(perfoma.getText().toString());
                    }

                    if (!perfoma.getText().toString().equals("")) {
                        postOrderModel.setPerfoma_percentage(perfoma.getText().toString());
                    }

                    if (!order_remark.getText().toString().equals("")) {
                        postOrderModel.setRemark(order_remark.getText().toString());
                    }

                    //Post order to server
                    Log.d("", "");
                    progress_bar.setVisibility(View.VISIBLE);

                    if(isQuotation){
                        new ServiceAPI(CreateOrderActivty.this).postQuotation(postOrderModel).enqueue(new Callback<OrderModel>() {
                            @Override
                            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                                progress_bar.setVisibility(View.GONE);
                                if (response.isSuccessful()) {
                                    Toast.makeText(CreateOrderActivty.this,"Quotation created successfully",Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(CreateOrderActivty.this, OrderDetailsActivity.class);
                                    intent.putExtra("order_id",response.body().getId());
                                    intent.putExtra("is_quotation",true);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    StaticFunctions.commonError(response, CreateOrderActivty.this);

                                }
                            }

                            @Override
                            public void onFailure(Call<OrderModel> call, Throwable t) {
                                progress_bar.setVisibility(View.GONE);
                                StaticFunctions.showAllFailureError(t,CreateOrderActivty.this);
                            }
                        });
                    }else{
                        new ServiceAPI(CreateOrderActivty.this).postOrderModel(postOrderModel).enqueue(new Callback<OrderModel>() {
                            @Override
                            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                                progress_bar.setVisibility(View.GONE);
                                if (response.isSuccessful()) {
                                    Toast.makeText(CreateOrderActivty.this,"Order placed successfully",Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(CreateOrderActivty.this, OrderDetailsActivity.class);
                                    intent.putExtra("order_id",response.body().getId());
                                    startActivity(intent);
                                    finish();
                                } else {
                                    StaticFunctions.commonError(response, CreateOrderActivty.this);

                                }
                            }

                            @Override
                            public void onFailure(Call<OrderModel> call, Throwable t) {
                                progress_bar.setVisibility(View.GONE);
                                StaticFunctions.showAllFailureError(t,CreateOrderActivty.this);
                            }
                        });
                    }

                }
            }
        });


    }

    private void setUpList() {

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        items_list_recyclerview.setLayoutManager(mLayoutManager);

        itemListAdapter = new ItemDetailListAdapter(CreateOrderActivty.this, lineItemModels, true,null);
        itemListAdapter.setRemoveItemListener(new ItemDetailListAdapter.RemoveItemListener() {
            @Override
            public void onItemRemove() {
                if(itemListAdapter.getItemCount()<1){
                    item_container.setVisibility(View.GONE);
                }
            }

            @Override
            public void onItemEdit(LineItemModel lineItemModel, final int position) {
                BottomSheetAddItem bottomSheetDialogFragment = new BottomSheetAddItem();
                Bundle bundle = new Bundle();
                bundle.putString("item_id",lineItemModel.getItem().getId());
                bundle.putString("item_unit_code",lineItemModel.getUnit_code());
                bundle.putString("item_qty", String.valueOf(lineItemModel.getQuantity()));
                bundle.putString("item_unit_rate", String.valueOf(lineItemModel.getUnit_rate()));
                bundle.putString("item_desc",lineItemModel.getDescription());
                bottomSheetDialogFragment.setArguments(bundle);

                bottomSheetDialogFragment.setAddItemListener(new BottomSheetAddItem.AddItemListener() {
                    @Override
                    public void onItemAdded(LineItemModel lineItemModel) {
                        lineItemModels.set(position,lineItemModel);
                        itemListAdapter.notifyItemChanged(position);

                    }
                });

                //show it
                bottomSheetDialogFragment.show(getSupportFragmentManager(), Constants.ADD_ITEM_DIALOG);
            }
        });
        items_list_recyclerview.setAdapter(itemListAdapter);

    }

    public void getCompanies() {
        companyModels.clear();

        new ServiceAPI(CreateOrderActivty.this).getCompanies().enqueue(new Callback<ArrayList<CompanyModel>>() {
            @Override
            public void onResponse(Call<ArrayList<CompanyModel>> call, Response<ArrayList<CompanyModel>> response) {
                if (response.isSuccessful()) {
                    companyModels.addAll(response.body());

                    spinnerAdapterCompanies = new SpinnerAdapterCompanies(CreateOrderActivty.this, R.layout.spinneritem, companyModels);
                    company_spinner.setAdapter(spinnerAdapterCompanies);

                } else {
                    StaticFunctions.commonError(response, CreateOrderActivty.this);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CompanyModel>> call, Throwable t) {
                StaticFunctions.showAllFailureError(t, CreateOrderActivty.this);

            }
        });

    }


    private boolean validate() {
        if (((CompanyModel) company_spinner.getSelectedItem()) == null) {
            Toast.makeText(CreateOrderActivty.this, "Please select company", Toast.LENGTH_LONG).show();
            return false;
        }

        if (lineItemModels.size() < 1) {
            Toast.makeText(CreateOrderActivty.this, "Please select atleast one item", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;

    }


}
