package com.abdeveloper.maniartradelink.orders;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.abdeveloper.maniartradelink.BaseActivity;
import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.adapter.ItemListAdapter;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.orders.adapter.ItemDetailListAdapter;
import com.abdeveloper.maniartradelink.orders.model.CompanyModel;
import com.abdeveloper.maniartradelink.orders.model.LineItemModel;
import com.abdeveloper.maniartradelink.orders.model.OrderModel;
import com.abdeveloper.maniartradelink.orders.model.PostOrderModel;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.krishna.fileloader.FileLoader;
import com.krishna.fileloader.listener.FileRequestListener;
import com.krishna.fileloader.pojo.FileResponse;
import com.krishna.fileloader.request.FileLoadRequest;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 6/2/19.
 */

public class OrderDetailsActivity extends BaseActivity {


    @BindView(R.id.items_list_recyclerview)
    RecyclerView items_list_recyclerview;

    @BindView(R.id.company_name)
    TextView company_name;

    @BindView(R.id.company_email)
    TextView company_email;

    @BindView(R.id.company_phone)
    TextView company_phone;

    @BindView(R.id.compan_gst)
    TextView compan_gst;

    @BindView(R.id.company_address)
    TextView company_address;

    @BindView(R.id.description_container)
    CardView description_container;

    @BindView(R.id.description_text)
    TextView description_text;

    @BindView(R.id.total_price)
    TextView total_price;

    @BindView(R.id.csgst_tax)
    TextView csgst_tax;

    @BindView(R.id.sgst_tax)
    TextView sgst_tax;


    @BindView(R.id.igst_tax)
    TextView igst_tax;

    @BindView(R.id.total_price_with_tax)
    TextView total_price_with_tax;

    @BindView(R.id.main_progress_bar)
    ProgressBar main_progress_bar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    //Order status
    @BindView(R.id.btn_common_action)
    AppCompatButton btn_common_action;

    @BindView(R.id.btn_cancel)
    AppCompatButton btn_cancel;


    @BindView(R.id.btn_partial_dispatch)
    AppCompatButton btn_partial_dispatch;


    /*@BindView(R.id.btn_delivered)
    AppCompatButton btn_delivered;

    @BindView(R.id.btn_dispatch)
    AppCompatButton btn_dispatch;


    @BindView(R.id.btn_ready_to_dispatch)
    AppCompatButton btn_ready_to_dispatch;*/

    @BindView(R.id.order_status_action_layout)
    CardView order_status_action_layout;

    @BindView(R.id.status_container)
    CardView status_container;

    @BindView(R.id.status)
    TextView status_textView;

    @BindView(R.id.btn_print_challan)
    AppCompatButton btn_print_challan;

    String ORDER_ID = null;
    String CHALLAN_ID = null;
    String ORDER_STATUS = null;


    ArrayList<LineItemModel> lineItemModels = new ArrayList<>();

    ItemDetailListAdapter itemListAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_details_layout);

        ButterKnife.bind(this);
        setUpList();

        if (getIntent().getBooleanExtra("is_quotation", false)) {
            toolbar.setTitle("Quotation Details");
            getQuotationFromServer();
        } else {
            toolbar.setTitle("Order Details");
            getOrderFromServer();
        }

        toolbar.setTitleTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.white));
    }


    private void setUpList() {

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        items_list_recyclerview.setLayoutManager(mLayoutManager);

    }

    private void getOrderFromServer() {

        if (getIntent() != null) {
            if (getIntent().getStringExtra("order_id") != null) {

                //Call server
                new ServiceAPI(OrderDetailsActivity.this).getOrderDetail(getIntent().getStringExtra("order_id")).enqueue(new Callback<OrderModel>() {
                    @Override
                    public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                        main_progress_bar.setVisibility(View.GONE);
                        if (response.isSuccessful()) {
                            renderView(response);

                        } else {
                            StaticFunctions.commonError(response, OrderDetailsActivity.this);
                        }
                    }

                    @Override
                    public void onFailure(Call<OrderModel> call, Throwable t) {
                        main_progress_bar.setVisibility(View.GONE);
                        StaticFunctions.showAllFailureError(t, OrderDetailsActivity.this);

                    }
                });


            } else {
                finish();
            }
        } else {
            finish();
        }


    }

    private void renderView(Response<OrderModel> response) {
        lineItemModels.clear();

        ORDER_STATUS = response.body().getStatus();

        ORDER_ID = response.body().getId();

        //Set company
        setCompanyLayout(response.body().getCompany());

        //Set Items
        setItemsLayout(response.body().getLine());

        //Set Description
        setOrderDescription(response.body().getDescription());

        //Set Order Totals
        setOrderTotals(response.body());

        //Setting buttons
        setStatusAction(response.body().getStatus(),response);

        btn_print_challan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                main_progress_bar.setVisibility(View.VISIBLE);
                FileLoader.with(OrderDetailsActivity.this)
                        .load(Constants.BASE_URL+"/api/challan/pdf?id="+CHALLAN_ID,false) //2nd parameter is optioal, pass true to force load from network
                        .fromDirectory("ManiarTradeLink", FileLoader.DIR_INTERNAL)
                        .asFile(new FileRequestListener<File>() {
                            @Override
                            public void onLoad(FileLoadRequest request, FileResponse<File> response) {
                                main_progress_bar.setVisibility(View.GONE);

                                File loadedFile = response.getBody();
                                StaticFunctions.printFile(OrderDetailsActivity.this,loadedFile);
                                // do something with the file
                            }

                            @Override
                            public void onError(FileLoadRequest request, Throwable t) {
                                Log.d("","");
                                main_progress_bar.setVisibility(View.GONE);

                            }
                        });
            }
        });
    }

    private void setStatusAction(String status,Response<OrderModel> response) {
        btn_partial_dispatch.setVisibility(View.GONE);
        if (!status.equals(Constants.CANCELLED)) {
            order_status_action_layout.setVisibility(View.VISIBLE);
            status_container.setVisibility(View.VISIBLE);
            switch (status) {
                case Constants.PENDING:
                    btn_common_action.setText(getString(R.string.status_approved));
                    status_textView.setText(Constants.PENDING);
                    status_textView.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this,R.color.colorRed));

                    break;
                case Constants.APPROVED:
                    btn_common_action.setText(getString(R.string.status_ready_to_dispatch));
                    status_textView.setText(Constants.APPROVED);
                    status_textView.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this,R.color.green));


                    break;
                case Constants.READY_TO_DISPATCH:
                    btn_common_action.setText(getString(R.string.status_dispatch));
                    btn_partial_dispatch.setVisibility(View.VISIBLE);
                    status_textView.setText(Constants.READY_TO_DISPATCH);
                    status_textView.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this,R.color.green));


                    break;
                case Constants.PARTIAL_DISPATCH:
                    btn_common_action.setVisibility(View.VISIBLE);
                    btn_common_action.setText(getString(R.string.status_delivered));


                    btn_partial_dispatch.setVisibility(View.VISIBLE);
                    status_textView.setText(Constants.PARTIAL_DISPATCH);
                    status_textView.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this,R.color.green));

                    if(response.body().getChallan_id()!=null){
                        CHALLAN_ID = response.body().getChallan_id();
                        btn_print_challan.setVisibility(View.VISIBLE);
                    }else{
                        btn_print_challan.setVisibility(View.GONE);
                    }

                    break;
                case Constants.DISPATCH:
                    btn_common_action.setText(getString(R.string.status_delivered));
                    btn_cancel.setVisibility(View.GONE);
                    status_textView.setText(Constants.DISPATCH);
                    status_textView.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this,R.color.green));

                    if(response.body().getChallan_id()!=null){
                        CHALLAN_ID = response.body().getChallan_id();
                        btn_print_challan.setVisibility(View.VISIBLE);
                    }else{
                        btn_print_challan.setVisibility(View.GONE);
                    }

                    break;
                case Constants.DELIVERED:
                    order_status_action_layout.setVisibility(View.GONE);
                    status_textView.setText(Constants.DELIVERED);
                    status_textView.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this,R.color.green));


                    break;
            }
        }else{
            status_container.setVisibility(View.VISIBLE);
            status_textView.setText(Constants.CANCELLED);
            status_textView.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this,R.color.colorRed));

        }




        //Button Clicks
        btn_common_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String status = btn_common_action.getText().toString().toLowerCase();
                if(status.equals("approve")){
                    status = Constants.APPROVED;
                }
                if(status.equals("dispatch")){
                    status = Constants.DISPATCH;
                }
                patchOrderStatus(status);
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                patchOrderStatus(Constants.CANCELLED);
            }
        });

        btn_partial_dispatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Open Partial Dispatch Dialog
                BottomSheetPartialDispatch bottomSheetPartialDispatch = new BottomSheetPartialDispatch();
                Bundle bundle = new Bundle();
                bundle.putString("order_id",ORDER_ID);
                bundle.putParcelableArrayList("partial_dispatch_item_list",lineItemModels);
                bottomSheetPartialDispatch.setArguments(bundle);
                bottomSheetPartialDispatch.setItemListener(new BottomSheetPartialDispatch.ItemPatchListener() {
                    @Override
                    public void onItemPatched() {
                        getOrderFromServer();
                    }
                });

                //show it
                bottomSheetPartialDispatch.show(getSupportFragmentManager(), Constants.ADD_ITEM_DIALOG);
            }
        });

    }

    private void patchOrderStatus(String status) {
        main_progress_bar.setVisibility(View.VISIBLE);
        PostOrderModel postOrderModel = new PostOrderModel();
        postOrderModel.setStatus(status);
        new ServiceAPI(OrderDetailsActivity.this).patchOrderStatus(ORDER_ID,postOrderModel).enqueue(new Callback<OrderModel>() {
            @Override
            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                main_progress_bar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    renderView(response);
                } else {
                    StaticFunctions.commonError(response, OrderDetailsActivity.this);

                }
            }

            @Override
            public void onFailure(Call<OrderModel> call, Throwable t) {
                main_progress_bar.setVisibility(View.GONE);
                StaticFunctions.showAllFailureError(t, OrderDetailsActivity.this);

            }
        });
    }

    private void getQuotationFromServer() {

        if (getIntent() != null) {
            if (getIntent().getStringExtra("order_id") != null) {

                lineItemModels.clear();

                //Call server
                new ServiceAPI(OrderDetailsActivity.this).getQuotationDetails(getIntent().getStringExtra("order_id")).enqueue(new Callback<OrderModel>() {
                    @Override
                    public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                        main_progress_bar.setVisibility(View.GONE);
                        if (response.isSuccessful()) {

                            //THIS VIEW IS FOR QUOTATIONS

                            //Set company
                            setCompanyLayout(response.body().getCompany());

                            //Set Items
                            setItemsLayout(response.body().getLine());

                            //Set Description
                            setOrderDescription(response.body().getDescription());

                            //Set Order Totals
                            setOrderTotals(response.body());

                        } else {
                            StaticFunctions.commonError(response, OrderDetailsActivity.this);
                        }
                    }

                    @Override
                    public void onFailure(Call<OrderModel> call, Throwable t) {
                        main_progress_bar.setVisibility(View.GONE);
                        StaticFunctions.showAllFailureError(t, OrderDetailsActivity.this);

                    }
                });


            } else {
                finish();
            }
        } else {
            finish();
        }


    }


    private void setOrderTotals(OrderModel body) {

        Double quotation_total_value = 0.0;
        Double quotation_taxable_value = 0.0;
        Double quotation_final_amount = 0.0;

        Double cgst_value = 0.0;
        Double sgst_value = 0.0;
        Double igst_value = 0.0;

        for (int i = 0; i < body.getLine().size(); i++) {

            //If status is delivered then check if it was partial dispatch by checking the total dispatched quantity
            //if total dispatch quantity is not similar to total quantity than create order against this quantity
            Integer quantity = body.getLine().get(i).getQuantity();

            if(body.getStatus()!=null && body.getStatus().equals(Constants.DELIVERED)){
                if(body.getLine().get(i).getTotal_dispatched_quantity()!=null && body.getLine().get(i).getTotal_dispatched_quantity()>0) {
                    if (!body.getLine().get(i).getTotal_dispatched_quantity().equals(quantity)) {
                        quantity = body.getLine().get(i).getTotal_dispatched_quantity();
                    }
                }
            }

            Double total_value = quantity * body.getLine().get(i).getUnit_rate();
            Double taxable_value = (Double.parseDouble(body.getLine().get(i).getItem().getCategoryModel().getTax()) / 100) * total_value;
            Double final_amount = total_value + taxable_value;

            quotation_total_value = quotation_total_value + total_value;
            quotation_taxable_value = quotation_taxable_value + taxable_value;
            quotation_final_amount = quotation_final_amount + final_amount;
        }

        total_price.setText(Constants.RUPEE + StaticFunctions.formatBigDouble(quotation_total_value));
        total_price_with_tax.setText(Constants.RUPEE + StaticFunctions.formatBigDouble(quotation_final_amount));


        if (body.getTax().equals("cgst/sgst")) {
            cgst_value = quotation_taxable_value / 2;
            sgst_value = quotation_taxable_value / 2;

            csgst_tax.setText((Constants.RUPEE + StaticFunctions.formatBigDouble(cgst_value)));
            sgst_tax.setText((Constants.RUPEE + StaticFunctions.formatBigDouble(sgst_value)));

        } else {
            igst_value = quotation_taxable_value;
            igst_tax.setText((Constants.RUPEE + StaticFunctions.formatBigDouble(igst_value)));
        }


    }

    private void setOrderDescription(String description) {

        if (description != null) {
            description_container.setVisibility(View.VISIBLE);
            description_text.setText(description);
        } else {
            description_container.setVisibility(View.GONE);
        }
    }

    private void setItemsLayout(ArrayList<LineItemModel> line) {
        lineItemModels.addAll(line);
        itemListAdapter = new ItemDetailListAdapter(OrderDetailsActivity.this, lineItemModels, false,ORDER_STATUS);
        items_list_recyclerview.setAdapter(itemListAdapter);

    }

    private void setCompanyLayout(CompanyModel company) {

        company_name.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(getString(R.string.company_name) + " ", OrderDetailsActivity.this), "\n", StaticFunctions.setSpannableText(company.getCompany_name().toString() + "", OrderDetailsActivity.this)));
        company_email.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(getString(R.string.company_email) + " ", OrderDetailsActivity.this), "\n", StaticFunctions.setSpannableText(company.getEmail(), OrderDetailsActivity.this)));
        company_phone.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(getString(R.string.company_phone) + " ", OrderDetailsActivity.this), "\n", StaticFunctions.setSpannableText(company.getPhone(), OrderDetailsActivity.this)));
        company_address.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(getString(R.string.company_address) + " ", OrderDetailsActivity.this), "\n", StaticFunctions.setSpannableText(company.getLine_address_1(), OrderDetailsActivity.this)));
        compan_gst.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(getString(R.string.company_gst_no) + " ", OrderDetailsActivity.this), "\n", StaticFunctions.setSpannableText(company.getGst_no(), OrderDetailsActivity.this)));

    }
}
