package com.abdeveloper.maniartradelink.orders.adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.orders.model.LineItemModel;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;

import java.util.ArrayList;

/**
 * Created by root on 6/2/19.
 */

public class ItemDetailListAdapter  extends RecyclerView.Adapter<ItemDetailListAdapter.MyViewHolder> {

    private AppCompatActivity mActivity;
    private ArrayList<LineItemModel> mlist;
    Boolean fromOrderCreationPage = false;
    String ORDER_STATUS=null;

    public interface RemoveItemListener{
        public void onItemRemove();
        public void onItemEdit(LineItemModel lineItemModel, int position);

    }

    RemoveItemListener removeItemListener;

    public void setRemoveItemListener(RemoveItemListener removeItemListener){
        this.removeItemListener = removeItemListener;
    }


    public ItemDetailListAdapter(AppCompatActivity mActivity, ArrayList<LineItemModel> cartModelArrayList, boolean fromOrderCreationPage, String ORDER_STATUS) {
        this.mActivity = mActivity;
        this.mlist = cartModelArrayList;
        this.fromOrderCreationPage =fromOrderCreationPage;
        this.ORDER_STATUS = ORDER_STATUS;
    }

    @Override
    public ItemDetailListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_detail_layout, parent, false);
        return new ItemDetailListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final ItemDetailListAdapter.MyViewHolder holder, final int position) {

        StringBuilder item_name = new StringBuilder();

        if(mlist.get(position).getItem().getCategoryModel()!=null){
            item_name.append(mlist.get(position).getItem().getName()+" - "+mlist.get(position).getItem().getCategoryModel().getHsn_code()+"\n");
        }else{
            item_name.append(mlist.get(position).getItem().getName()+"\n");
        }

        holder.item_name.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item)+" ",mActivity),"\n",StaticFunctions.setSpannableText(item_name.toString()+"",mActivity)));
        holder.item_price.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_price) + " ", mActivity), "\n", StaticFunctions.setSpannableText(String.valueOf(mlist.get(position).getUnit_rate()), mActivity)));


        if(mlist.get(position).getDescription()!=null && !mlist.get(position).getDescription().equals("")) {
            holder.description_container.setVisibility(View.VISIBLE);
            holder.item_description.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_description) + " ", mActivity), "\n", StaticFunctions.setSpannableText(mlist.get(position).getDescription(), mActivity)));
        }else{
            holder.description_container.setVisibility(View.GONE);
        }

        if(position==mlist.size()-1){
            holder.line_seperator.setVisibility(View.GONE);
        }else{
            holder.line_seperator.setVisibility(View.VISIBLE);
        }



        holder.remove_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mlist.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, getItemCount());
                removeItemListener.onItemRemove();
            }
        });

        holder.edit_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeItemListener.onItemEdit(mlist.get(position),position);
            }
        });


        Double amount = (mlist.get(position).getUnit_rate() * mlist.get(position).getQuantity());
        holder.item_qty.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_qty) + " ", mActivity), "\n", StaticFunctions.setSpannableText(String.valueOf(mlist.get(position).getQuantity()), mActivity)));

        //setting quantity that is actually delivered
        if(ORDER_STATUS!=null && ORDER_STATUS.equals(Constants.DELIVERED)) {
            if (mlist.get(position).getTotal_dispatched_quantity() != null && mlist.get(position).getTotal_dispatched_quantity() > 0) {
                holder.item_qty.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_qty) + " ", mActivity), "\n", StaticFunctions.setSpannableText(String.valueOf(mlist.get(position).getTotal_dispatched_quantity()), mActivity)));
                amount = (mlist.get(position).getUnit_rate() * mlist.get(position).getTotal_dispatched_quantity());
            }
        }


        //showing total dispatched quantity
        if(ORDER_STATUS!=null && ORDER_STATUS.equals(Constants.PARTIAL_DISPATCH)) {
            if (mlist.get(position).getTotal_dispatched_quantity() != null && mlist.get(position).getTotal_dispatched_quantity() > 0) {
                holder.item_qty_dispatched.setVisibility(View.VISIBLE);
                holder.item_qty_dispatched.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_qty_dispatched_text) + " ", mActivity), "\n", StaticFunctions.setSpannableText(String.valueOf(mlist.get(position).getTotal_dispatched_quantity()), mActivity)));

            } else {
                holder.item_qty_dispatched.setVisibility(View.GONE);
            }
        }

        //If user is on Order details page
        if(!fromOrderCreationPage) {
            holder.item_total.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_amount) + " ", mActivity), "\n", StaticFunctions.setSpannableText(String.valueOf("\u20B9" + amount), mActivity)));
            holder.action_container.setVisibility(View.GONE);
        }else{
            //If user is on Order creation page
            holder.item_total.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_unit) + " ", mActivity), "\n", StaticFunctions.setSpannableText(mlist.get(position).getUnit_code(), mActivity)));
            holder.action_container.setVisibility(View.VISIBLE);
        }



    }





    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView item_name,item_qty,item_price,item_total, item_description,item_qty_dispatched;
        LinearLayout main_container,action_container,description_container;
        View line_seperator;
        AppCompatButton remove_item,edit_item;

        public MyViewHolder(View view) {
            super(view);
            item_name = view.findViewById(R.id.item_name);
            item_qty = view.findViewById(R.id.item_qty);
            main_container = (LinearLayout) view.findViewById(R.id.main_container);
            action_container = (LinearLayout) view.findViewById(R.id.action_container);
            description_container = (LinearLayout) view.findViewById(R.id.description_container);
            item_price = view.findViewById(R.id.item_price);
            item_total = view.findViewById(R.id.item_total);
            item_description = view.findViewById(R.id.item_description);
            item_qty_dispatched = view.findViewById(R.id.item_qty_dispatched);
            line_seperator = view.findViewById(R.id.line_seperator);
            remove_item = view.findViewById(R.id.remove_item);
            edit_item = view.findViewById(R.id.edit_item);

        }
    }


}
