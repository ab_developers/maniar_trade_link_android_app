package com.abdeveloper.maniartradelink.orders.adapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.orders.OrderDetailsActivity;
import com.abdeveloper.maniartradelink.orders.model.OrderModel;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;

import java.util.ArrayList;

/**
 * Created by root on 5/2/19.
 */

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.MyViewHolder> {

    private AppCompatActivity mActivity;
    private ArrayList<OrderModel> mlist;
    Boolean isQuotation;

    public OrderListAdapter(AppCompatActivity mActivity, ArrayList<OrderModel> cartModelArrayList, Boolean isQuotation) {
        this.mActivity = mActivity;
        this.mlist = cartModelArrayList;
        this.isQuotation = isQuotation;
    }

    @Override
    public OrderListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_list_item_layout, parent, false);
        return new OrderListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final OrderListAdapter.MyViewHolder holder, final int position) {

        StringBuilder total_items = new StringBuilder();
        for(int i=0;i<mlist.get(position).getLine().size();i++){
            total_items.append(mlist.get(position).getLine().get(i).getQuantity()+" x "+mlist.get(position).getLine().get(i).getItem().getName()+ " - "+mlist.get(position).getLine().get(i).getItem().getCategoryModel().getHsn_code()+"\n");
        }

        holder.total_items.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item)+" ",mActivity),"\n",StaticFunctions.setSpannableText(total_items.toString()+"",mActivity)));
        holder.company_name.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.company_name)+" ",mActivity),"\n",StaticFunctions.setSpannableText(mlist.get(position).getCompany().getCompany_name(),mActivity)));
        holder.date_placed.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.order_placed_date)+" ",mActivity),"\n",StaticFunctions.setSpannableText(StaticFunctions.currentIST(mlist.get(position).getCreated_on()),mActivity)));

        if(!isQuotation){
            holder.item_status.setVisibility(View.VISIBLE);
            holder.item_status.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_status)+" ",mActivity),"\n",StaticFunctions.setSpannableText(mlist.get(position).getStatus().toUpperCase(),mActivity)));
            holder.order_id.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.order_id_text)+" ",mActivity),"\n",StaticFunctions.setSpannableText("#"+mlist.get(position).getId(),mActivity)));

        }else{
            holder.item_status.setVisibility(View.GONE);
            holder.order_id.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.quotation_id_text)+" ",mActivity),"\n",StaticFunctions.setSpannableText("#"+mlist.get(position).getId(),mActivity)));

        }
        holder.main_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent intent = new Intent(mActivity, OrderDetailsActivity.class);
                    intent.putExtra("order_id", mlist.get(position).getId());
                    intent.putExtra("is_quotation",isQuotation);
                    mActivity.startActivity(intent);

            }
        });
    }





    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView item_status,date_placed,company_name,total_items, order_id;
        LinearLayout main_container;

        public MyViewHolder(View view) {
            super(view);
            total_items = view.findViewById(R.id.total_items);
            item_status = view.findViewById(R.id.item_status);
            main_container = (LinearLayout) view.findViewById(R.id.main_container);
            date_placed = view.findViewById(R.id.date_placed);
            company_name = view.findViewById(R.id.company_name);
            order_id = view.findViewById(R.id.order_id);

        }
    }


}
