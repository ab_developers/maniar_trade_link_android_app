package com.abdeveloper.maniartradelink.orders.adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.orders.model.LineItemModel;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

/**
 * Created by root on 7/2/19.
 */

public class PartialDispatchItemAdapter  extends RecyclerView.Adapter<PartialDispatchItemAdapter.MyViewHolder> {

    private AppCompatActivity mActivity;
    private ArrayList<LineItemModel> mlist;

    public PartialDispatchItemAdapter(AppCompatActivity mActivity, ArrayList<LineItemModel> cartModelArrayList) {
        this.mActivity = mActivity;
        this.mlist = cartModelArrayList;
    }

    @Override
    public PartialDispatchItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.partial_dispatch_list_item, parent, false);
        return new PartialDispatchItemAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final PartialDispatchItemAdapter.MyViewHolder holder, final int position) {

        StringBuilder total_items = new StringBuilder();
        total_items.append(mlist.get(position).getItem().getName());

        holder.total_items.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item)+" ",mActivity),"\n",StaticFunctions.setSpannableText(total_items.toString()+"",mActivity)));
        holder.item_qty.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_qty) + " ", mActivity), "\n", StaticFunctions.setSpannableText(String.valueOf(mlist.get(position).getQuantity()), mActivity)));

        Integer total_dispatched_quantity=0;
        if(mlist.get(position).getTotal_dispatched_quantity()!=null){
            total_dispatched_quantity = mlist.get(position).getTotal_dispatched_quantity();
        }

        holder.item_qty_dispatched.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.item_qty_dispatched) + " ", mActivity), "\n", StaticFunctions.setSpannableText(String.valueOf(total_dispatched_quantity), mActivity)));
        holder.item_qty_remaining.setText(TextUtils.concat(StaticFunctions.setSpannableTextLight(mActivity.getString(R.string.remaining_qty) + " ", mActivity), "\n", StaticFunctions.setSpannableText(String.valueOf(mlist.get(position).getQuantity()-total_dispatched_quantity), mActivity)));

        //So that user cannot enter value greater than the item that can be dispatched
        Integer remaining_quantity= mlist.get(position).getQuantity()-total_dispatched_quantity;

        //holder.edittext_partial_dispatch_qty.setFilters(new InputFilter[]{ new InputFilterMinMax(1, remaining_quantity,"Dispatch quantity cannot be more than "+remaining_quantity,mActivity)});

        holder.edittext_partial_dispatch_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!charSequence.toString().equals("")) {
                    mlist.get(position).setPartial_dispatched_quantity(Integer.valueOf(charSequence.toString()));
                }else{
                    mlist.get(position).setPartial_dispatched_quantity(0);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }





    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView item_qty,item_qty_dispatched, total_items,item_qty_remaining;
        LinearLayout main_container;
        MaterialEditText edittext_partial_dispatch_qty;

        public MyViewHolder(View view) {
            super(view);
            total_items = view.findViewById(R.id.total_items);
            item_qty = view.findViewById(R.id.item_qty);
            main_container = (LinearLayout) view.findViewById(R.id.main_container);
            item_qty_dispatched = view.findViewById(R.id.item_qty_dispatched);
            item_qty_remaining = view.findViewById(R.id.item_qty_remaining);
            edittext_partial_dispatch_qty = view.findViewById(R.id.edittext_partial_dispatch_qty);

        }
    }


}

