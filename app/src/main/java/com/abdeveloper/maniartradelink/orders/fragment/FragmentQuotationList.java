package com.abdeveloper.maniartradelink.orders.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.orders.CreateOrderActivty;
import com.abdeveloper.maniartradelink.orders.OrderDetailsActivity;
import com.abdeveloper.maniartradelink.orders.adapter.OrderListAdapter;
import com.abdeveloper.maniartradelink.orders.model.OrderModel;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.abdeveloper.maniartradelink.utils.pagination.CommonPaginationClass;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 5/2/19.
 */

public class FragmentQuotationList extends Fragment {

    @BindView(R.id.order_list_recyclerview)
    RecyclerView order_list_recyclerview;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    ArrayList<OrderModel> orderModels = new ArrayList<>();

    OrderListAdapter orderListAdapter;

    @BindView(R.id.create_order)
    FloatingActionButton create_order;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_list_layout, container, false);
        ButterKnife.bind(this,view);


        setUpList();
        new CommonPaginationClass(getActivity(),order_list_recyclerview,orderListAdapter,orderModels,true,"");
        //getDataFromServer();

        create_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), CreateOrderActivty.class);
                intent.putExtra("is_quotation",true);
                startActivity(intent);
            }
        });

        return view;
    }

    /*private void getDataFromServer() {

        orderModels.clear();

        //Call server
        new ServiceAPI(getContext()).getQuotations().enqueue(new Callback<ArrayList<OrderModel>>() {
            @Override
            public void onResponse(Call<ArrayList<OrderModel>> call, Response<ArrayList<OrderModel>> response) {
                progress_bar.setVisibility(View.GONE);
                if(response.isSuccessful() && isAdded()){
                    orderModels.addAll(response.body());
                    orderListAdapter = new OrderListAdapter((AppCompatActivity) getActivity(),orderModels,true);
                    order_list_recyclerview.setAdapter(orderListAdapter);
                }else{
                    if(isAdded())
                        StaticFunctions.commonError(response, getActivity());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<OrderModel>> call, Throwable t) {
                progress_bar.setVisibility(View.GONE);
                StaticFunctions.showAllFailureError(t,getActivity());

            }
        });


    }*/

    private void setUpList() {

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        order_list_recyclerview.setLayoutManager(mLayoutManager);

        orderListAdapter = new OrderListAdapter((AppCompatActivity) getActivity(),orderModels,true);
        order_list_recyclerview.setAdapter(orderListAdapter);

    }
}
