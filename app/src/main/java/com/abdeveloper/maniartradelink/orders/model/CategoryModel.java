package com.abdeveloper.maniartradelink.orders.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 5/2/19.
 */

public class CategoryModel implements Parcelable{
    String id;
    String hsn_code;
    String tax;

    protected CategoryModel(Parcel in) {
        id = in.readString();
        hsn_code = in.readString();
        tax = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(hsn_code);
        dest.writeString(tax);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CategoryModel> CREATOR = new Creator<CategoryModel>() {
        @Override
        public CategoryModel createFromParcel(Parcel in) {
            return new CategoryModel(in);
        }

        @Override
        public CategoryModel[] newArray(int size) {
            return new CategoryModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHsn_code() {
        return hsn_code;
    }

    public void setHsn_code(String hsn_code) {
        this.hsn_code = hsn_code;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }
}
