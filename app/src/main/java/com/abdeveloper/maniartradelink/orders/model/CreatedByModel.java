package com.abdeveloper.maniartradelink.orders.model;

/**
 * Created by root on 5/2/19.
 */

public class CreatedByModel {

    private String id;

    private String user;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getUser ()
    {
        return user;
    }

    public void setUser (String user)
    {
        this.user = user;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", user = "+user+"]";
    }


}
