package com.abdeveloper.maniartradelink.orders.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 5/2/19.
 */

public class ItemDetailModel implements Parcelable{

    String id;
    String name;
    CategoryModel category;

    public ItemDetailModel() {
    }

    protected ItemDetailModel(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemDetailModel> CREATOR = new Creator<ItemDetailModel>() {
        @Override
        public ItemDetailModel createFromParcel(Parcel in) {
            return new ItemDetailModel(in);
        }

        @Override
        public ItemDetailModel[] newArray(int size) {
            return new ItemDetailModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryModel getCategoryModel() {
        return category;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.category = categoryModel;
    }
}
