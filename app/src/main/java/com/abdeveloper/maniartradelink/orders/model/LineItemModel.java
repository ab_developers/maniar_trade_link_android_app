package com.abdeveloper.maniartradelink.orders.model;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.abdeveloper.maniartradelink.home.model.ItemModel;

/**
 * Created by root on 5/2/19.
 */

public class LineItemModel implements Parcelable{


    //we are not sending partial_dispatched_quantity in parcelable because then it
    // create issue in partial dispatch bottom sheet

    ItemDetailModel item;
    String id;
    Integer quantity;
    Integer partial_dispatched_quantity;
    Integer total_dispatched_quantity;
    Double unit_rate;
    String unit_code;
    String description;

    public LineItemModel() {
    }

    public LineItemModel(Parcel in) {
        id = in.readString();
        if (in.readByte() == 0) {
            quantity = null;
        } else {
            quantity = in.readInt();
        }

        if (in.readByte() == 0) {
            total_dispatched_quantity = null;
        } else {
            total_dispatched_quantity = in.readInt();
        }
        if (in.readByte() == 0) {
            unit_rate = null;
        } else {
            unit_rate = in.readDouble();
        }
        unit_code = in.readString();
        description = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        if (quantity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(quantity);
        }

        if (total_dispatched_quantity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(total_dispatched_quantity);
        }
        if (unit_rate == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(unit_rate);
        }
        dest.writeString(unit_code);
        dest.writeString(description);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LineItemModel> CREATOR = new Creator<LineItemModel>() {
        @Override
        public LineItemModel createFromParcel(Parcel in) {
            return new LineItemModel(in);
        }

        @Override
        public LineItemModel[] newArray(int size) {
            return new LineItemModel[size];
        }
    };

    public ItemDetailModel getItem() {
        return item;
    }

    public void setItem(ItemDetailModel item) {
        this.item = item;
    }

    public String getUnit_code() {
        return unit_code;
    }

    public void setUnit_code(String unit_code) {
        this.unit_code = unit_code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPartial_dispatched_quantity() {
        return partial_dispatched_quantity;
    }

    public void setPartial_dispatched_quantity(Integer partial_dispatched_quantity) {
        this.partial_dispatched_quantity = partial_dispatched_quantity;
    }

    public Integer getTotal_dispatched_quantity() {
        return total_dispatched_quantity;
    }

    public void setTotal_dispatched_quantity(Integer total_dispatched_quantity) {
        this.total_dispatched_quantity = total_dispatched_quantity;
    }

    public Double getUnit_rate() {
        return unit_rate;
    }

    public void setUnit_rate(Double unit_rate) {
        this.unit_rate = unit_rate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
