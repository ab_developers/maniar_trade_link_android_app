package com.abdeveloper.maniartradelink.orders.model;

import com.abdeveloper.maniartradelink.home.model.ItemModel;

import java.util.ArrayList;

/**
 * Created by root on 5/2/19.
 */

public class OrderModel {

    private ArrayList<LineItemModel> item;

    private String last_modified_on;

    private String created_on;

    private String perfoma_percentage;

    private String description;

    private CompanyModel company;

    private String tax;

    private String remark;

    private String id;

    private CreatedByModel created_by;

    private String status;

    private String challan_id;

    public String getChallan_id() {
        return challan_id;
    }

    public void setChallan_id(String challan_id) {
        this.challan_id = challan_id;
    }

    public ArrayList<LineItemModel> getLine() {
        return item;
    }

    public void setLine(ArrayList<LineItemModel> item) {
        this.item = item;
    }

    public String getLast_modified_on ()
    {
        return last_modified_on;
    }

    public void setLast_modified_on (String last_modified_on)
    {
        this.last_modified_on = last_modified_on;
    }

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getPerfoma_percentage ()
    {
        return perfoma_percentage;
    }

    public void setPerfoma_percentage (String perfoma_percentage)
    {
        this.perfoma_percentage = perfoma_percentage;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public CompanyModel getCompany ()
    {
        return company;
    }

    public void setCompany (CompanyModel company)
    {
        this.company = company;
    }

    public String getTax ()
    {
        return tax;
    }

    public void setTax (String tax)
    {
        this.tax = tax;
    }

    public String getRemark ()
    {
        return remark;
    }

    public void setRemark (String remark)
    {
        this.remark = remark;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public CreatedByModel getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (CreatedByModel created_by)
    {
        this.created_by = created_by;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [item = "+item+", last_modified_on = "+last_modified_on+", created_on = "+created_on+", perfoma_percentage = "+perfoma_percentage+", description = "+description+", company = "+company+", tax = "+tax+", remark = "+remark+", id = "+id+", created_by = "+created_by+", status = "+status+"]";
    }
}
