package com.abdeveloper.maniartradelink.orders.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 6/2/19.
 */

public class PostLineItemModel {

    String item;
    String item_name;
    String id;
    Integer quantity;
    Integer partial_dispatched_quantity;
    Integer total_dispatched_quantity;
    Double unit_rate;
    String unit_code;
    String description;


    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPartial_dispatched_quantity() {
        return partial_dispatched_quantity;
    }

    public void setPartial_dispatched_quantity(Integer partial_dispatched_quantity) {
        this.partial_dispatched_quantity = partial_dispatched_quantity;
    }

    public Integer getTotal_dispatched_quantity() {
        return total_dispatched_quantity;
    }

    public void setTotal_dispatched_quantity(Integer total_dispatched_quantity) {
        this.total_dispatched_quantity = total_dispatched_quantity;
    }

    public Double getUnit_rate() {
        return unit_rate;
    }

    public void setUnit_rate(Double unit_rate) {
        this.unit_rate = unit_rate;
    }

    public String getUnit_code() {
        return unit_code;
    }

    public void setUnit_code(String unit_code) {
        this.unit_code = unit_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
