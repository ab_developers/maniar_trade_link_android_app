package com.abdeveloper.maniartradelink.orders.model;

import java.util.ArrayList;

/**
 * Created by root on 6/2/19.
 */

public class PostOrderModel {

    private ArrayList<PostLineItemModel> item;

    private String perfoma_percentage;

    private String description;

    private String company;

    private String tax;

    private String remark;

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<PostLineItemModel> getItem() {
        return item;
    }

    public void setItem(ArrayList<PostLineItemModel> item) {
        this.item = item;
    }

    public String getPerfoma_percentage() {
        return perfoma_percentage;
    }

    public void setPerfoma_percentage(String perfoma_percentage) {
        this.perfoma_percentage = perfoma_percentage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
