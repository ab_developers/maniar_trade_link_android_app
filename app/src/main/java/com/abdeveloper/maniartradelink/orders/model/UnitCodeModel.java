package com.abdeveloper.maniartradelink.orders.model;

/**
 * Created by root on 6/2/19.
 */

public class UnitCodeModel {
    String id;
    String name;

    public UnitCodeModel(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
