package com.abdeveloper.maniartradelink.retrofit;

import java.util.ArrayList;

/**
 * Created by root on 20/7/17.
 */

public class APIError {

    private ArrayList<String> non_field_errors;

    private ArrayList<String> text;

    private ArrayList<String> shipping;

    private ArrayList<String> phone_number;

    private ArrayList<String> phone;

    private ArrayList<String> username;

    private ArrayList<String> name;

    private ArrayList<String> new_password1;
    private ArrayList<String> password1;
    private ArrayList<String> new_password2;
    private ArrayList<String> password2;
    private ArrayList<String> old_password;

    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ArrayList<String> getPassword1() {
        return password1;
    }

    public void setPassword1(ArrayList<String> password1) {
        this.password1 = password1;
    }

    public ArrayList<String> getPassword2() {
        return password2;
    }

    public void setPassword2(ArrayList<String> password2) {
        this.password2 = password2;
    }

    public ArrayList<String> getOld_password() {
        return old_password;
    }

    public void setOld_password(ArrayList<String> old_password) {
        this.old_password = old_password;
    }

    public ArrayList<String> getNew_password1() {
        return new_password1;
    }

    public void setNew_password1(ArrayList<String> new_password1) {
        this.new_password1 = new_password1;
    }

    public ArrayList<String> getNew_password2() {
        return new_password2;
    }

    public void setNew_password2(ArrayList<String> new_password2) {
        this.new_password2 = new_password2;
    }

    public ArrayList<String> getName() {
        return name;
    }

    public void setName(ArrayList<String> name) {
        this.name = name;
    }

    public ArrayList<String> getUsername() {
        return username;
    }

    public void setUsername(ArrayList<String> username) {
        this.username = username;
    }

    public ArrayList<String> getPhone() {
        return phone;
    }

    public void setPhone(ArrayList<String> phone) {
        this.phone = phone;
    }

    public ArrayList<String> getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(ArrayList<String> phone_number) {
        this.phone_number = phone_number;
    }

    public ArrayList<String> getNon_field_errors() {
        return non_field_errors;
    }

    public void setNon_field_errors(ArrayList<String> non_field_errors) {
        this.non_field_errors = non_field_errors;
    }

    public ArrayList<String> getText() {
        return text;
    }

    public void setText(ArrayList<String> text) {
        this.text = text;
    }

    public ArrayList<String> getShipping() {
        return shipping;
    }

    public void setShipping(ArrayList<String> shipping) {
        this.shipping = shipping;
    }
}


