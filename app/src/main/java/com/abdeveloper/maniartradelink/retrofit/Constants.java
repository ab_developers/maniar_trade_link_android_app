package com.abdeveloper.maniartradelink.retrofit;


/**
 * Created by root on 29/8/17.
 */

public final class Constants {

    //DEBUG
    //public static final String BASE_URL = "https://test.tool-jack.com";
    //public static final String TELR_KEY = BuildConfig.TELR_KEY_DEBUG;

    //PROD
    public static final String BASE_URL = "http://13.233.58.216";



    //retrofit caching
    public static final String CACHE_CONTROL = "Cache-Control";
    public static final int MAX_AGE = 20;
    public static final int MAX_STALE = 120;
    public static final int ENQUIRY_DETAIL_CODE = 120;
    public static final int ENQUIRY__PART_DETAIL_CODE = 35;

    // FRAGMENT TAGS
    public static final String FRAGMENT_STOCKS_TAG = "fragment_stocks";
    public static final String FRAGMENT_PEDNING_ORDERS_TAG = "fragment_pending_orders";
    public static final String FRAGMENT_DISPATCH_ORDERS_ORDERS_TAG = "fragment_dispatch_orders";
    public static final String FRAGMENT_ORDERS_TAG = "fragment_orders";
    public static final String FRAGMENT_QUOTATION_TAG = "fragment_quotations";
    public static final String FRAGMENT_LEAD_TAG = "fragment_leads";
    public static final String FRAGMENT_MEETING_CREATE_TAG = "fragment_meeting_create";
    public static final String FRAGMENT_MEETINGS= "fragment_meetings";


    public static final String RUPEE = "\u20B9";
    public static final String ADD_ITEM_DIALOG = "add_item_dialog";


    //Order Status
    public static final String PENDING = "pending";
    public static final String APPROVED = "approved";
    public static final String CANCELLED = "cancelled";
    public static final String READY_TO_DISPATCH = "ready to dispatch";
    public static final String PARTIAL_DISPATCH = "partially dispatched";
    public static final String DISPATCH = "dispatched";
    public static final String DELIVERED = "delivered";

    //Meeting status
    //Order Status
    public static final String MEETING_PENDING = "pending";
    public static final String MEETING_STARTED = "started";
    public static final String MEETING_CANCELLED = "cancelled";
    public static final String MEETING_COMPLETED = "completed";


    public static final String ORDER_STATUS = "order_status";
    public static final java.lang.String DATE_FORMAT = "dd/MM/yyyy hh:mm aa";
    public static final int LOCATION_REQUEST_CODE = 282;
    public static final java.lang.String API_KEY = "AIzaSyAydn7WY5pD7WrfRBFNsGJTSXiC8JiJT7U";
    public static final String MEETING_ID = "meeting_id";
    public static final String BOTTOM_SHEET_ADD_REMARK = "Bottom_sheet_add_remark";
}


