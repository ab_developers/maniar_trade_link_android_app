package com.abdeveloper.maniartradelink.retrofit;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.abdeveloper.maniartradelink.ApplicationClass;
import com.abdeveloper.maniartradelink.BuildConfig;
import com.abdeveloper.maniartradelink.utils.storage.MySharedPreferences;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.abdeveloper.maniartradelink.retrofit.Constants.BASE_URL;

/**
 * Created by ubuntu on 19/3/17.
 */

public class RetroFitServiceGenerator {

    MySharedPreferences sharedPreferences;

    public  Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());
    public Retrofit retrofit = builder.build();
    public  HttpLoggingInterceptor logging = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    /*    private  Retrofit.Builder builderGeneric =
                new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(buildGsonConverter());*/
    private Context context;

    // private  Retrofit retrofitGeneric = builderGeneric.build();
    private OkHttpClient.Builder httpClient =
            new OkHttpClient.Builder();

    public RetroFitServiceGenerator(Context context) {
        this.context = context;
    }

    public RetroFitServiceGenerator() {
    }

    public static void getTokenNReload() {

          /*  //code get avaiable bon apeto accounts
            AccountManager mAccountManager = AccountManager.get(context);
            final Account availableAccounts[] = mAccountManager.getAccountsByType(AUTH_ACCOUNT_TYPE);
            SocialLoginActivity getTokenFun = new SocialLoginActivity();

            getTokenFun.getExistingAccountAuthToken(availableAccounts[0], AUTH_TOKEN_TYPE);*/

    }

    public <S> S serviceNOAuth(
            Class<S> serviceClass) {

        //common error toast bar
        // httpClient.addInterceptor(provideErrorInterceptor());
        httpClient.addInterceptor(logging);
        builder.client(httpClient.build());
        retrofit = builder.build();
        return retrofit.create(serviceClass);
    }


    public <S> S serviceWithAuth(
            Class<S> serviceClass) {

        //Getting ResponseToken Locally
        sharedPreferences = new MySharedPreferences(context);
        ApplicationClass.token = sharedPreferences.getToken();


        if (!TextUtils.isEmpty(ApplicationClass.token)) {
            AuthenticationInterceptor tokeninterceptor =
                    new AuthenticationInterceptor(ApplicationClass.token);

            if (!httpClient.interceptors().contains(tokeninterceptor)) {
                httpClient.addInterceptor(tokeninterceptor);

                if (BuildConfig.DEBUG) {
                    // do something for a debug build
                    httpClient.addInterceptor(logging);
                }

                //timeout
                httpClient.readTimeout(120, TimeUnit.SECONDS)
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(120, TimeUnit.SECONDS);

                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        } else
            getTokenNReload();

        return retrofit.create(serviceClass);
    }


    public <S> S serviceWithAuthLocalToken(
            Class<S> serviceClass, String token) {


        if (!TextUtils.isEmpty(token)) {
            AuthenticationInterceptor tokeninterceptor =
                    new AuthenticationInterceptor(token);

            if (!httpClient.interceptors().contains(tokeninterceptor)) {
                httpClient.addInterceptor(tokeninterceptor);

                if (BuildConfig.DEBUG) {
                    // do something for a debug build
                    httpClient.addInterceptor(logging);
                }

                //timeout
                httpClient.readTimeout(120, TimeUnit.SECONDS)
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(120, TimeUnit.SECONDS);

                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        } else
            getTokenNReload();

        return retrofit.create(serviceClass);
    }


    public <S> S createServiceNoCache(
            Class<S> serviceClass) {
        if (!TextUtils.isEmpty(ApplicationClass.token)) {
            AuthenticationInterceptor tokeninterceptor =
                    new AuthenticationInterceptor(ApplicationClass.token);

            if (!httpClient.interceptors().contains(tokeninterceptor)) {
                httpClient.addInterceptor(tokeninterceptor);

                if (BuildConfig.DEBUG) {
                    // do something for a debug build
                    httpClient.addInterceptor(logging);
                }
                //common error toast bar
                httpClient.addInterceptor(provideErrorInterceptor());

                /* for Cache */
                httpClient.addInterceptor(provideOfflineCacheInterceptor(0));
                httpClient.addNetworkInterceptor(provideCacheInterceptor(0));
                httpClient.cache(provideCache());
                /* for cache end */

                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        } else
            getTokenNReload();

        return retrofit.create(serviceClass);
    }

    public <S> S createServiceNoAuthWithCache(
            Class<S> serviceClass) {

        if (BuildConfig.DEBUG) {
            // do something for a debug build
            httpClient.addInterceptor(logging);
        }
        //common error toast bar
        httpClient.addInterceptor(provideErrorInterceptor());

                /* for Cache */
        httpClient.addInterceptor(provideOfflineCacheInterceptor(Constants.MAX_STALE));
        httpClient.addNetworkInterceptor(provideCacheInterceptor(Constants.MAX_AGE));
        httpClient.cache(provideCache());
                /* for cache end */

        builder.client(httpClient.build());
        retrofit = builder.build();


        return retrofit.create(serviceClass);
    }


    public <S> S createServiceNoAuthNoCache(
            Class<S> serviceClass) {

        if (BuildConfig.DEBUG) {
            // do something for a debug build
            httpClient.addInterceptor(logging);
        }
        //common error toast bar
        httpClient.addInterceptor(provideErrorInterceptor());

        builder.client(httpClient.build());
        retrofit = builder.build();


        return retrofit.create(serviceClass);
    }



   /* public  <S> S createServiceGeneric(
            Class<S> serviceClass) {
        if (!TextUtils.isEmpty(Application_Singleton.token)) {
            AuthenticationInterceptor tokeninterceptor =
                    new AuthenticationInterceptor(Application_Singleton.token);

            if (!httpClient.interceptors().contains(tokeninterceptor)) {
                httpClient.addInterceptor(tokeninterceptor);

                if (BuildConfig.DEBUG) {
                    // do something for a debug build
                    httpClient.addInterceptor(logging);
                }
                //common error toast bar
                httpClient.addInterceptor(provideErrorInterceptor());

                *//* for Cache *//*
                httpClient.addInterceptor(provideOfflineCacheInterceptor(MAX_STALE));
                httpClient.addNetworkInterceptor(provideCacheInterceptor(MAX_AGE));
                httpClient.cache(provideCache());
                *//* for cache end *//*

                builderGeneric.client(httpClient.build());
                retrofitGeneric = builderGeneric.build();
            }
        }
        else
            getTokenNReload();

        return retrofitGeneric.create(serviceClass);
    }*/


    private Cache provideCache() {
        Cache cache = null;
        try {
            cache = new Cache(new File(context.getCacheDir(), "http-cache"),
                    10 * 1024 * 1024); // 10 MB
        } catch (Exception e) {
            Log.e("Error", "Could not create Cache File!");
        }
        return cache;
    }


    public Interceptor provideCacheInterceptor(final int minutes) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());

                // re-write response header to force use of cache
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxAge(minutes, TimeUnit.MINUTES)
                        .build();

                return response.newBuilder()
                        .header(Constants.CACHE_CONTROL, cacheControl.toString())
                        .build();
            }
        };
    }

    public void backgroundThreadShortToast(final Context context,
                                           final String msg) {
        if (context != null && msg != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();

                }
            });
        }
    }


    public Interceptor provideErrorInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());

                if (!response.isSuccessful()) {

                    backgroundThreadShortToast(context, "Status : " + response.code() + " Msg: " + response.message());


                } else {

                }


                return response;
            }
        };
    }

    public Interceptor provideOfflineCacheInterceptor(final int minutes) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();

                CacheControl cacheControl = new CacheControl.Builder()
                        .maxStale(minutes, TimeUnit.MINUTES)
                        .build();

                request = request.newBuilder()
                        .cacheControl(cacheControl)
                        .build();
                return chain.proceed(request);
            }
        };
    }

    /*private  GsonConverterFactory buildGsonConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        // Adding custom deserializers
        gsonBuilder.registerTypeAdapter(FeedItemModel.class, new FeedItemModelDeserializer());
        Gson myGson = gsonBuilder.create();

        return GsonConverterFactory.create(myGson);
    }*/

}