package com.abdeveloper.maniartradelink.retrofit;



import com.abdeveloper.maniartradelink.auth.model.PostLoginModel;
import com.abdeveloper.maniartradelink.auth.model.ResponseToken;
import com.abdeveloper.maniartradelink.home.model.FCMModel;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.home.model.LeadModel;
import com.abdeveloper.maniartradelink.meeting.model.MeetingModel;
import com.abdeveloper.maniartradelink.orders.model.CompanyModel;
import com.abdeveloper.maniartradelink.orders.model.OrderModel;
import com.abdeveloper.maniartradelink.orders.model.PostOrderModel;
import com.abdeveloper.maniartradelink.orders.model.UnitCodeModel;
import com.abdeveloper.maniartradelink.utils.storage.UserProfile;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by root on 29/8/17.
 */

public interface RetrofitAPI {
     /*Retrofit get annotation with our URL
       And our method that will return us the list ob Book
    */

    static final String API_VERSION = "/api";

    //GET
    @GET(API_VERSION + "/items/")
    Call<ArrayList<ItemModel>> getItems();

    @GET(API_VERSION + "/meetings/")
    Call<ArrayList<MeetingModel>> getMeetings();

    @GET(API_VERSION + "/meetings/{id}/")
    Call<MeetingModel> getMeetingDetails(@Path("id") String id);



    @GET(API_VERSION + "/unit_code/")
    Call<ArrayList<UnitCodeModel>> getUnitCode();

   @GET(API_VERSION + "/company/")
    Call<ArrayList<CompanyModel>> getCompanies();

    @GET(API_VERSION + "/orders/")
    Call<ArrayList<OrderModel>> getOrders(@Query("limit") int limit, @Query("offset") int offset, @Query("status") String status,@Query("ordering") String ordering);

    @GET(API_VERSION + "/orders/{id}/")
    Call<OrderModel> getOrderDetail(@Path("id") String id);

    @GET(API_VERSION + "/quotations/")
    Call<ArrayList<OrderModel>> getQuotations(@Query("limit") int limit, @Query("offset") int offset, @Query("ordering") String ordering);

    @GET(API_VERSION + "/quotations/{id}/")
    Call<OrderModel> getQuotationDetails(@Path("id") String id);


    @GET(API_VERSION + "/user/")
    Call<UserProfile> getUserProfile();

    //POST
    @POST(API_VERSION + "/login/")
    Call<ResponseToken> postLogin(@Body PostLoginModel postLoginModel);

    @POST(API_VERSION + "/orders/")
    Call<OrderModel> postOrder(@Body PostOrderModel postOrderModel);

    @POST(API_VERSION + "/quotations/")
    Call<OrderModel> postQuotation(@Body PostOrderModel postOrderModel);

    @POST(API_VERSION + "/fcm_devices/")
    Call<FCMModel> postFCMTokenToServer(@Body FCMModel postLoginModel);

    @POST(API_VERSION + "/fcm_devices/{id}/")
    Call<ResponseBody> removeFCMTokenToServer(@Body FCMModel postLoginModel,@Path("id") String id);

    @POST(API_VERSION + "/leads/")
    Call<LeadModel> postLeads(@Body LeadModel leadModel);

    @POST(API_VERSION + "/meetings/")
    Call<MeetingModel> postMeetings(@Body MeetingModel meetingModel);

    //PATCH
    @PATCH(API_VERSION + "/orders/{id}/")
    Call<OrderModel> patchOrderStatus(@Path("id") String id,@Body PostOrderModel postOrderModel);

    @PATCH(API_VERSION + "/meetings/{id}/")
    Call<MeetingModel> patchMeetingDetails(@Path("id") String id,@Body MeetingModel meetingModel);


}
