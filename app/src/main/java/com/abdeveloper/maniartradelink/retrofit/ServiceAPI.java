package com.abdeveloper.maniartradelink.retrofit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;


import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.auth.model.PostLoginModel;
import com.abdeveloper.maniartradelink.auth.model.ResponseToken;
import com.abdeveloper.maniartradelink.home.DashboardActivity;
import com.abdeveloper.maniartradelink.home.model.FCMModel;
import com.abdeveloper.maniartradelink.home.model.ItemModel;
import com.abdeveloper.maniartradelink.home.model.LeadModel;
import com.abdeveloper.maniartradelink.meeting.model.MeetingModel;
import com.abdeveloper.maniartradelink.orders.model.CompanyModel;
import com.abdeveloper.maniartradelink.orders.model.OrderModel;
import com.abdeveloper.maniartradelink.orders.model.PostOrderModel;
import com.abdeveloper.maniartradelink.orders.model.UnitCodeModel;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.abdeveloper.maniartradelink.utils.retrofit_error.ErrorUtils;
import com.abdeveloper.maniartradelink.utils.storage.MySharedPreferences;
import com.abdeveloper.maniartradelink.utils.storage.UserProfile;
import com.abdeveloper.maniartradelink.utils.storage.UserProfileLocal;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 18/12/17.
 */

public class ServiceAPI {

    MySharedPreferences sharedPreferences;
    Context context;
    public Callback<UserProfile> callback;

    public ServiceAPI(Context context) {
        this.context = context;
        sharedPreferences = new MySharedPreferences(context);
    }


    public Call<ArrayList<ItemModel>> getItems() {
        RetrofitAPI service = new RetroFitServiceGenerator(context).createServiceNoAuthNoCache(RetrofitAPI.class);
        Call<ArrayList<ItemModel>> call = service.getItems();
        return call;
    }


    public Call<ArrayList<UnitCodeModel>> getUnitCode() {
        RetrofitAPI service = new RetroFitServiceGenerator(context).createServiceNoAuthNoCache(RetrofitAPI.class);
        Call<ArrayList<UnitCodeModel>> call = service.getUnitCode();
        return call;
    }

    public Call<ArrayList<CompanyModel>> getCompanies() {
        RetrofitAPI service = new RetroFitServiceGenerator(context).createServiceNoAuthNoCache(RetrofitAPI.class);
        Call<ArrayList<CompanyModel>> call = service.getCompanies();
        return call;
    }

    public Call<ArrayList<OrderModel>> getOrders(int limit, int offset, String status) {
        RetrofitAPI service = new RetroFitServiceGenerator(context).createServiceNoAuthNoCache(RetrofitAPI.class);
        Call<ArrayList<OrderModel>> call = service.getOrders(limit, offset, status, "id");
        return call;
    }

    public Call<OrderModel> getOrderDetail(String id) {
        RetrofitAPI service = new RetroFitServiceGenerator(context).createServiceNoAuthNoCache(RetrofitAPI.class);
        Call<OrderModel> call = service.getOrderDetail(id);
        return call;
    }

    public Call<OrderModel> postOrderModel(PostOrderModel postOrderModel) {
        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceWithAuth(RetrofitAPI.class);
        Call<OrderModel> call = service.postOrder(postOrderModel);
        return call;
    }

    public Call<OrderModel> patchOrderStatus(String order_id, PostOrderModel postOrderModel) {
        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceWithAuth(RetrofitAPI.class);
        Call<OrderModel> call = service.patchOrderStatus(order_id, postOrderModel);
        return call;
    }

    public Call<ArrayList<OrderModel>> getQuotations(int limit, int offset) {
        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceWithAuth(RetrofitAPI.class);
        Call<ArrayList<OrderModel>> call = service.getQuotations(limit, offset, "id");
        return call;
    }

    public Call<OrderModel> getQuotationDetails(String id) {
        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceWithAuth(RetrofitAPI.class);
        Call<OrderModel> call = service.getQuotationDetails(id);
        return call;
    }

    public Call<OrderModel> postQuotation(PostOrderModel postOrderModel) {
        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceWithAuth(RetrofitAPI.class);
        Call<OrderModel> call = service.postQuotation(postOrderModel);
        return call;
    }


    public Call<LeadModel> postLeads(LeadModel leadModel) {
        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceWithAuth(RetrofitAPI.class);
        Call<LeadModel> call = service.postLeads(leadModel);
        return call;
    }

    public Call<MeetingModel> postMeetings(MeetingModel meetingModel) {
        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceWithAuth(RetrofitAPI.class);
        Call<MeetingModel> call = service.postMeetings(meetingModel);
        return call;
    }

    public Call<ArrayList<MeetingModel>> getMeetings() {
        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceWithAuth(RetrofitAPI.class);
        Call<ArrayList<MeetingModel>> call = service.getMeetings();
        return call;
    }

    public Call<MeetingModel> getMeetingDetails(String id) {
        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceWithAuth(RetrofitAPI.class);
        Call<MeetingModel> call = service.getMeetingDetails(id);
        return call;
    }

    public Call<MeetingModel> patchMeetingDetails(String id,MeetingModel meetingModel) {
        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceWithAuth(RetrofitAPI.class);
        Call<MeetingModel> call = service.patchMeetingDetails(id,meetingModel);
        return call;
    }

    //Posting Login
    public void postLogin(String username, String password) {
        //While the app fetched data we are displaying a progress dialog
        final ProgressDialog loading = ProgressDialog.show(context, context.getString(R.string.loggin_in), context.getString(R.string.please_wait_text), false, false);


        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceNOAuth(RetrofitAPI.class);

        PostLoginModel googletoken = new PostLoginModel();
        googletoken.setLogin(username);
        googletoken.setPassword(password);

        Call<ResponseToken> call = service.postLogin(googletoken);
        call.enqueue(new Callback<ResponseToken>() {
            @Override
            public void onResponse(Call<ResponseToken> call, Response<ResponseToken> response) {
                loading.dismiss();
                if (response.isSuccessful()) {
                    //Dismissing the loading progressbar
                    sharedPreferences.saveToken(response.body().getToken());

                    //saveFirebaseToken


                    getUserProfile(true);

                } else {

                    // parse the response body …
                    APIError error = ErrorUtils.parseError(response);
                    // … and use it to show error information

                    // … or just log the issue like we’re doing :)
//                        Log.d("error message", error.message());
                    if (error != null) {

                        if (error.getNon_field_errors() != null) {
                            StaticFunctions.errorDialog(context, context.getString(R.string.error), error.getNon_field_errors().get(0));
                        } else {
                            StaticFunctions.somethingWentWrong(context);
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseToken> call, Throwable t) {
                // something went completely south (like no internet connection)
                loading.dismiss();
                StaticFunctions.showAllFailureError(t, context);

            }
        });

    }

    //getting user Profile
    private void getUserProfile(final Boolean saveFirebaseToken) {
        //While the app fetched data we are displaying a progress dialog
        final ProgressDialog progressDialog = ProgressDialog.show(context, context.getString(R.string.fetching_data_text), context.getString(R.string.please_wait_text), false, false);

        //if first time than show progressDialog

        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceWithAuth(RetrofitAPI.class);
        Call<UserProfile> call = service.getUserProfile();

        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {

                        new UserProfileLocal(context).setEmployee_position(response.body().getEmployee_position());
                        new UserProfileLocal(context).setJoin_date(response.body().getJoin_date());
                        new UserProfileLocal(context).setUsername(response.body().getUser().getUsername());
                        new UserProfileLocal(context).setEmail(response.body().getUser().getEmail());
                        new UserProfileLocal(context).setFull_name(response.body().getUser().getFull_name());
                        new UserProfileLocal(context).setPhone(response.body().getUser().getPhone());
                        new UserProfileLocal(context).setUser_type(response.body().getUser().getUser_type());

                        //Save Firebase Token to server
                        if (saveFirebaseToken)
                            postTokenToServer(response.body().getUser().getUsername());

                        Intent i = new Intent(context, DashboardActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(i);

                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {
                // something went completely south (like no internet connection)
                progressDialog.dismiss();
                StaticFunctions.showAllFailureError(t, context);

            }
        });
    }

    private void postTokenToServer(final String username) {

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get new Instance ID token
                        final String token = task.getResult().getToken();
                        FCMModel fcmModel = new FCMModel();
                        fcmModel.setDev_id(username);
                        fcmModel.setReg_id(token);

                        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceNOAuth(RetrofitAPI.class);
                        Call<FCMModel> call = service.postFCMTokenToServer(fcmModel);
                        call.enqueue(new Callback<FCMModel>() {
                            @Override
                            public void onResponse(Call<FCMModel> call, Response<FCMModel> response) {
                                if (response.isSuccessful()) {
                                    new MySharedPreferences(context).saveFirebaseTokenID(response.body().getId());
                                    new MySharedPreferences(context).saveFirebaseToken(token);

                                }
                            }

                            @Override
                            public void onFailure(Call<FCMModel> call, Throwable t) {

                            }
                        });

                    }
                });

    }

    public void removeTokenToServer(Context context) {

        MySharedPreferences sharedPreferences = new MySharedPreferences(context);

        String tokenID = sharedPreferences.getFirebaseTokenID();

        FCMModel fcmModel = new FCMModel();
        fcmModel.setDev_id(new UserProfileLocal(context).getUsername());
        fcmModel.setReg_id(new MySharedPreferences(context).getFirebaseToken());
        fcmModel.setIs_active(false);

        RetrofitAPI service = new RetroFitServiceGenerator(context).serviceNOAuth(RetrofitAPI.class);
        Call<ResponseBody> call = service.removeFCMTokenToServer(fcmModel,tokenID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("Response",response.body().toString());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }


}
