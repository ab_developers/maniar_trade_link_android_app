package com.abdeveloper.maniartradelink.utils;

/**
 * Created by root on 7/2/19.
 */

import android.content.Context;
import android.text.InputFilter;
import android.text.Spanned;
import android.widget.Toast;

public class InputFilterMinMax implements InputFilter {

    private int min, max;
    private String message;
    private Context context;

    public InputFilterMinMax(int min, int max,String message,Context context) {
        this.min = min;
        this.max = max;
        this.message = message;
        this.context = context;
    }


    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            int input = Integer.parseInt(dest.toString() + source.toString());
            if (isInRange(min, max, input)) {
                return null;
            }else{
                Toast.makeText(context,message,Toast.LENGTH_LONG).show();
            }
        } catch (NumberFormatException nfe) { }
        return "";
    }

    private boolean isInRange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}
