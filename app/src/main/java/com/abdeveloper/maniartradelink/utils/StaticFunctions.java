package com.abdeveloper.maniartradelink.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.retrofit.APIError;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.utils.retrofit_error.ErrorUtils;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.threeten.bp.Instant;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Response;

/**
 * Created by root on 3/1/19.
 */

public class StaticFunctions {


    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);


        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }


    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, Activity context) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int reqHeight = displayMetrics.heightPixels;
        int reqWidth = displayMetrics.widthPixels;

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public static int getImage(String imageName, Context context) {

        int drawableResourceId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());

        return drawableResourceId;
    }

    public static SharedPreferences getAppSharedPreferences(Context applicationContext) {
        return applicationContext.getSharedPreferences("maniartradelink", Context.MODE_PRIVATE);
    }

    public static Spannable setSpannableTextLight(String text, Context mActivity) {
        Spannable wordtoSpan = new SpannableString(text.toUpperCase());
        wordtoSpan.setSpan(new RelativeSizeSpan(0.8f), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.black_alpha_40)), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return  wordtoSpan;
    }


    public static Spannable setSpannableText(String text,Context mActivity) {
        Spannable wordtoSpan = new SpannableString(text);

        wordtoSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity,R.color.jet)), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return  wordtoSpan;
    }

    public static void commonError(Response<?> response, Context context) {
        // parse the response body …
        APIError error = ErrorUtils.parseError(response);
        // … and use it to show error information

        // … or just log the issue like we’re doing :)
//                        Log.d("error message", error.message());
        if (error != null) {

            if (error.getNon_field_errors() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.error), error.getNon_field_errors().get(0));
            } else if (error.getPhone_number() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.number_format_error), error.getPhone_number().get(0));
            } else if (error.getPhone() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.number_format_error), error.getPhone().get(0));
            }else if (error.getUsername() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.username), error.getUsername().get(0));
            }else if (error.getName() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.name), error.getName().get(0));
            }else if (error.getNew_password1() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.password_error), error.getNew_password1().get(0));
            }else if (error.getNew_password2() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.password_error), error.getNew_password2().get(0));
            }else if (error.getOld_password() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.password_error), context.getString(R.string.old_password_error_text));
            }else if (error.getPassword1() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.password_error),error.getPassword1().get(0));
            }else if (error.getPassword2() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.password_error), error.getPassword2().get(0));
            }
            else if (error.getOld_password() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.password_error), context.getString(R.string.old_password_error_text));
            }
            else if (error.getText() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.error), error.getText().get(0));
            } else if (error.getText() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.error), error.getText().get(0));
            } else if (error.getShipping() != null) {
                StaticFunctions.errorDialog(context, context.getString(R.string.error), error.getShipping().get(0));
            } else {
                StaticFunctions.somethingWentWrong(context);
            }
        }
    }

    public static void errorDialog(Context context, String string, String message) {
        if (context != null) {
            new MaterialDialog.Builder(context).title(string)
                    .content(message)
                    .positiveText(context.getString(R.string.ok_text))
                    .show();
        }
    }

    public static void somethingWentWrong(Context context) {
        if (context != null) {
            new MaterialDialog.Builder(context).title(context.getString(R.string.error))
                    .content(context.getString(R.string.something_went_wrong))
                    .positiveText(context.getString(R.string.ok_text))
                    .show();
        }
    }

    public static void internetConnectionIssue(Context context) {
        new MaterialDialog.Builder(context).title(R.string.internet_error_title_text)
                .content(R.string.internet_error_content_text)
                .positiveText(context.getString(R.string.ok_text))
                .show();
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static void showAllFailureError(Throwable t, Context context) {
        if (t != null) {
            if(context!=null) {
                if (StaticFunctions.isOnline(context)) {
                    if(t.getMessage()!=null) {
                        StaticFunctions.showFailureError(((Activity) context).getWindow().getDecorView().getRootView(), t.getMessage());
                    }else{
                        StaticFunctions.showFailureError(((Activity) context).getWindow().getDecorView().getRootView(), context.getString(R.string.something_went_wrong));
                    }
                } else {
                    StaticFunctions.showFailureError(((Activity) context).getWindow().getDecorView().getRootView(), context.getString(R.string.internet_error_title_text));
                }
            }else{
                if(((Activity) context).getWindow().getDecorView().getRootView()!=null)
                    StaticFunctions.showFailureError(((Activity) context).getWindow().getDecorView().getRootView(), context.getString(R.string.something_went_wrong));
            }
        }
    }

    public static void showFailureError(View rootView, String message) {
        Snackbar.make(rootView,message,Snackbar.LENGTH_LONG).show();
    }




    public static String currentIST(String dateFromServer) {

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX");
            Instant dateResult = OffsetDateTime.parse(dateFromServer, formatter).toInstant();
            dateFromServer = dateResult.toString();
        }catch (Exception e){
            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX");
                Instant dateResult = OffsetDateTime.parse(dateFromServer, formatter).toInstant();
                dateFromServer = dateResult.toString();
            }catch (Exception e2){
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssXXX");
                Instant dateResult = OffsetDateTime.parse(dateFromServer, formatter).toInstant();
                dateFromServer = dateResult.toString();
            }
        }


        int calendar = Calendar.getInstance().get(Calendar.YEAR);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat sdfLocal = new SimpleDateFormat("EEE, d MMM yyyy, h:mm a", Locale.getDefault());
        SimpleDateFormat sdfLocal1 = new SimpleDateFormat("EEE, d MMM, h:mm a", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            Date date = sdf.parse(dateFromServer);
            if ((date.getYear() + 1900) == calendar) {
                return sdfLocal1.format(date);
            } else {
                return sdfLocal.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            try {
                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                sdf1.setTimeZone(TimeZone.getTimeZone("GMT"));
                return sdfLocal.format(sdf1.parse(dateFromServer));

            } catch (Exception e1) {
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
                sdf2.setTimeZone(TimeZone.getTimeZone("GMT"));
                try {
                    return sdfLocal.format(sdf2.parse(dateFromServer));
                } catch (ParseException e2) {

                    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSX");
                    sdf3.setTimeZone(TimeZone.getTimeZone("GMT"));
                    try {
                        return sdfLocal.format(sdf3.parse(dateFromServer));
                    } catch (ParseException e3) {
                        e3.printStackTrace();
                    }

                }
            }

        }

        return "";

    }


    public static String currentUTC(String dateFromServer){

        SimpleDateFormat sdf1 = new SimpleDateFormat(Constants.DATE_FORMAT);
        Date time = null;
        try {
            time = sdf1.parse(dateFromServer);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }


        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        return outputFmt.format(time);

    }

    public static String formatBigDouble(double data) {
        return String.format(Locale.getDefault(), "%.2f", data).replace("٫", ".");
    }

    public static String formatBigDoubleWithoutLocale(double data) {
        return String.format("%.2f", data).replace("٫", ".");
    }


    public static Double roundingLocation(double location) {

        return BigDecimal.valueOf(location)
                .setScale(5, RoundingMode.HALF_UP)
                .doubleValue();

    }

    public static void printFile(Context context, final File file){
        PrintDocumentAdapter pda = new PrintDocumentAdapter() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, android.os.CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras) {
                if (cancellationSignal.isCanceled()) {
                    callback.onLayoutCancelled();
                    return;
                }


                PrintDocumentInfo pdi = new PrintDocumentInfo.Builder("printing_doc").setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT).build();

                callback.onLayoutFinished(pdi, true);
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, android.os.CancellationSignal cancellationSignal, WriteResultCallback callback) {
                FileInputStream input = null;
                FileOutputStream output = null;

                try {

                    input = new FileInputStream(file);
                    output = new FileOutputStream(destination.getFileDescriptor());

                    byte[] buf = new byte[1024];
                    int bytesRead;

                    while ((bytesRead = input.read(buf)) > 0) {
                        output.write(buf, 0, bytesRead);
                    }

                    callback.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});

                } catch (FileNotFoundException ee) {
                    //Catch exception
                } catch (Exception e) {
                    //Catch exception
                } finally {
                    try {
                        input.close();
                        output.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


        };

        PrintManager printManager = (PrintManager) context.getSystemService(Context.PRINT_SERVICE);
        String jobName = context.getString(R.string.app_name) + " Document";
        printManager.print(jobName, pda, null);
    }
}
