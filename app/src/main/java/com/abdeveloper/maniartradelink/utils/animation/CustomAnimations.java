package com.abdeveloper.maniartradelink.utils.animation;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.abdeveloper.maniartradelink.R;
import com.github.florent37.viewanimator.ViewAnimator;

/**
 * Created by root on 3/1/19.
 */

public class CustomAnimations {

    Context context;

    public CustomAnimations(Context context) {
        this.context = context;
    }

    public CustomAnimations() {
    }

    public Animation fadeOut(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.fadeout);
    }

    public void splashScreenAnimatino(View image, View text){
        ViewAnimator
                .animate(image)
                .translationY(-1000, 0)
                .alpha(0,1)
                .andAnimate(text)
                .dp().translationX(-20, 0)
                .decelerate()
                .duration(1000)
                .start();
    }

    public void loginScreenAnimation(View diagonal_view, View btn_phone_number, View btn_facebook){
        ViewAnimator
                .animate(diagonal_view)
                .alpha(0,1)
                .duration(1000)
                .andAnimate(btn_phone_number)
                .translationX(-1000, 0)
                .alpha(0,1)
                .duration(1500)
                .andAnimate(btn_facebook)
                .alpha(0,1)
                .translationX(1000, 0)
                .duration(1500)
                .start();
    }


}
