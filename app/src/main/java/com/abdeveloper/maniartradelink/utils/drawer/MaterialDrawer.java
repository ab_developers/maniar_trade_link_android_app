package com.abdeveloper.maniartradelink.utils.drawer;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.abdeveloper.maniartradelink.ApplicationClass;
import com.abdeveloper.maniartradelink.OpenContainer;
import com.abdeveloper.maniartradelink.auth.LoginActivity;
import com.abdeveloper.maniartradelink.home.DashboardActivity;
import com.abdeveloper.maniartradelink.R;
import com.abdeveloper.maniartradelink.home.PagesWebViewActivity;
import com.abdeveloper.maniartradelink.home.fragment.FragmentLeads;
import com.abdeveloper.maniartradelink.meeting.fragment.FragmentMeetingList;
import com.abdeveloper.maniartradelink.orders.fragment.FragmentOrderList;
import com.abdeveloper.maniartradelink.orders.fragment.FragmentQuotationList;
import com.abdeveloper.maniartradelink.retrofit.Constants;
import com.abdeveloper.maniartradelink.retrofit.RetrofitAPI;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.storage.MySharedPreferences;
import com.abdeveloper.maniartradelink.utils.storage.UserProfileLocal;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

/**
 * Created by root on 9/12/17.
 */

public class MaterialDrawer {

    Drawer materialDrawer;
    AppCompatActivity context;
    Toolbar toolbar;
    Bitmap myHeaderBackground;
    Bitmap accountProfileDefault;
    Drawable headerBackground;
    Drawable accountProfile;

    MySharedPreferences sharedPreferences;


    public MaterialDrawer(AppCompatActivity context, Toolbar toolbar) {
        this.context = context;
        this.toolbar = toolbar;
        sharedPreferences = new MySharedPreferences(context);
    }

    public void setUpDrawer() {

        headerBackground = new BitmapDrawable(context.getResources(), myHeaderBackground);


        final ProfileDrawerItem drawerItem;
        drawerItem = new ProfileDrawerItem().withName(new UserProfileLocal(context).getFull_name()).withEmail(new UserProfileLocal(context).getEmail())
                .withIcon(R.drawable.ic_user)
                .withTextColorRes(R.color.colorAccent);

        //adding a guest account or User account
        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(context)
                .withHeaderBackground(R.color.colorPrimary)
                .withTextColor(ContextCompat.getColor(context, R.color.white))
                .withSelectionListEnabled(false)
                .withTranslucentStatusBar(true)
                .addProfiles(
                        drawerItem
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {

                        return true;
                    }
                })
                .build();

        //creating a drawer
        //create the drawer and remember the `Drawer` result object
        materialDrawer = new DrawerBuilder()
                .withActivity(context)
                .withToolbar(toolbar)
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {

                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {

                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                //.withSliderBackgroundColor(ContextCompat.getColor(context,R.color.colorPrimaryDark))
                .withAccountHeader(headerResult)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D

                        ////materialDrawer.closeDrawer();
                        Log.v("Test_Position",position+"");

                        if (position == 1) {
                            //when dialog is closed than open home page
                            //go to home
                            Intent i = new Intent(context, DashboardActivity.class);
                            // set the new task and clear flags
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(i);
                        }

                        if(position==2){

                            FragmentLeads fragmentLeads = new FragmentLeads();
                            ApplicationClass.CONTAINERFRAG = fragmentLeads;
                            ApplicationClass.CONTAINER_TITLE = context.getString(R.string.fragment_leads);
                            ApplicationClass.CONTAINERFRAGTAG = Constants.FRAGMENT_LEAD_TAG;


                            Intent intent1 = new Intent(context, OpenContainer.class);
                            intent1.putExtra("toolbarCategory", OpenContainer.COMMON_MENU);
                            context.startActivity(intent1);

                        }

                        if(position==4){
                            FragmentQuotationList fragmentOrderList = new FragmentQuotationList();


                            ApplicationClass.CONTAINERFRAG = fragmentOrderList;
                            ApplicationClass.CONTAINER_TITLE = context.getString(R.string.drawer_item_quotations);
                            ApplicationClass.CONTAINERFRAGTAG = Constants.FRAGMENT_QUOTATION_TAG;


                            Intent intent2 = new Intent(context, OpenContainer.class);
                            intent2.putExtra("toolbarCategory", OpenContainer.COMMON_MENU);
                            context.startActivity(intent2);

                        }

                        if(position==5){
                            FragmentOrderList fragmentOrderList = new FragmentOrderList();


                            ApplicationClass.CONTAINERFRAG = fragmentOrderList;
                            ApplicationClass.CONTAINER_TITLE = context.getString(R.string.drawer_item_orders);
                            ApplicationClass.CONTAINERFRAGTAG = Constants.FRAGMENT_ORDERS_TAG;


                            Intent intent2 = new Intent(context, OpenContainer.class);
                            intent2.putExtra("toolbarCategory", OpenContainer.COMMON_MENU);
                            context.startActivity(intent2);


                        }

                        if(position==7){

                            ApplicationClass.CONTAINERFRAG = new FragmentMeetingList();
                            ApplicationClass.CONTAINER_TITLE = context.getString(R.string.drawer_item_meetings);
                            ApplicationClass.CONTAINERFRAGTAG = Constants.FRAGMENT_MEETINGS;


                            Intent intent2 = new Intent(context, OpenContainer.class);
                            intent2.putExtra("toolbarCategory", OpenContainer.COMMON_MENU);
                            context.startActivity(intent2);
                        }

                        if(position==9){

                            Intent intent = new Intent(context, PagesWebViewActivity.class);
                            intent.putExtra("url", Constants.BASE_URL+"/calculator.html#");
                            intent.putExtra("title","Rate Calculator");
                            context.startActivity(intent);
                        }


                        if(position==10){

                            new ServiceAPI(context).removeTokenToServer(context);


                            new MySharedPreferences(context).ClearAllData();
                            Intent intent = new Intent(context,LoginActivity.class);


                            context.startActivity(intent);
                            context.finish();
                        }

                        return true;
                    }
                })
                .build();

        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1)
                //.withName(pricing_country+" / "+new UserProfile(context).getPricing_currency())
                .withName(context.getString(R.string.drawer_item_dashboard))
                .withIcon(ContextCompat.getDrawable(context, R.drawable.maniar_trade_link))
                .withIconTintingEnabled(true).withIconColorRes(R.color.colorPrimaryDark)
                .withSelectedIconColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .withSelectedTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        PrimaryDrawerItem item2 = new PrimaryDrawerItem().withIdentifier(2)
                //.withName(pricing_country+" / "+new UserProfile(context).getPricing_currency())
                .withName(context.getString(R.string.drawer_item_leads))
                .withIcon(ContextCompat.getDrawable(context, R.drawable.maniar_trade_link))
                .withIconTintingEnabled(true).withIconColorRes(R.color.colorPrimaryDark)
                .withSelectedIconColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .withSelectedTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        PrimaryDrawerItem item3 = new PrimaryDrawerItem().withIdentifier(3)
                //.withName(pricing_country+" / "+new UserProfile(context).getPricing_currency())
                .withName(context.getString(R.string.drawer_item_stocks))
                .withIcon(ContextCompat.getDrawable(context, R.drawable.maniar_trade_link))
                .withIconTintingEnabled(true).withIconColorRes(R.color.colorPrimaryDark)
                .withSelectedIconColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .withSelectedTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        PrimaryDrawerItem item4 = new PrimaryDrawerItem().withIdentifier(4)
                //.withName(pricing_country+" / "+new UserProfile(context).getPricing_currency())
                .withName(context.getString(R.string.drawer_item_quotations))
                .withIcon(ContextCompat.getDrawable(context, R.drawable.maniar_trade_link))
                .withIconTintingEnabled(true).withIconColorRes(R.color.colorPrimaryDark)
                .withSelectedIconColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .withSelectedTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        PrimaryDrawerItem item5 = new PrimaryDrawerItem().withIdentifier(5)
                //.withName(pricing_country+" / "+new UserProfile(context).getPricing_currency())
                .withName(context.getString(R.string.drawer_item_orders))
                .withIcon(ContextCompat.getDrawable(context, R.drawable.maniar_trade_link))
                .withIconTintingEnabled(true).withIconColorRes(R.color.colorPrimaryDark)
                .withSelectedIconColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .withSelectedTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        PrimaryDrawerItem item8 = new PrimaryDrawerItem().withIdentifier(8)
                //.withName(pricing_country+" / "+new UserProfile(context).getPricing_currency())
                .withName(context.getString(R.string.drawer_item_meetings))
                .withIcon(ContextCompat.getDrawable(context, R.drawable.maniar_trade_link))
                .withIconTintingEnabled(true).withIconColorRes(R.color.colorPrimaryDark)
                .withSelectedIconColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .withSelectedTextColor(ContextCompat.getColor(context, R.color.colorPrimary));


        PrimaryDrawerItem item6 = new PrimaryDrawerItem().withIdentifier(6)
                //.withName(pricing_country+" / "+new UserProfile(context).getPricing_currency())
                .withName(context.getString(R.string.drawer_item_profile))
                .withIcon(ContextCompat.getDrawable(context, R.drawable.maniar_trade_link))
                .withIconTintingEnabled(true).withIconColorRes(R.color.colorPrimaryDark)
                .withSelectedIconColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .withSelectedTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        PrimaryDrawerItem item9 = new PrimaryDrawerItem().withIdentifier(9)
                //.withName(pricing_country+" / "+new UserProfile(context).getPricing_currency())
                .withName(context.getString(R.string.drawer_item_rate_calculator))
                .withIcon(ContextCompat.getDrawable(context, R.drawable.maniar_trade_link))
                .withIconTintingEnabled(true).withIconColorRes(R.color.colorPrimaryDark)
                .withSelectedIconColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .withSelectedTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        PrimaryDrawerItem item7 = new PrimaryDrawerItem().withIdentifier(7)
                //.withName(pricing_country+" / "+new UserProfile(context).getPricing_currency())
                .withName(context.getString(R.string.drawer_item_logout))
                .withIcon(ContextCompat.getDrawable(context, R.drawable.maniar_trade_link))
                .withIconTintingEnabled(true).withIconColorRes(R.color.colorPrimaryDark)
                .withSelectedIconColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .withSelectedTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        materialDrawer.addItem(item1);
        materialDrawer.addItem(item2);
        materialDrawer.addItem(item3);
        materialDrawer.addItem(item4);
        materialDrawer.addItem(item5);
        materialDrawer.addItem(new DividerDrawerItem());
        materialDrawer.addItem(item8);
        materialDrawer.addItem(item6);
        materialDrawer.addItem(item9);
        materialDrawer.addItem(item7);



    }

    public Drawer getMaterialDrawer() {
        return materialDrawer;
    }


    public void closeDrawer() {
        materialDrawer.closeDrawer();
    }


}
