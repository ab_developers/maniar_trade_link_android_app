package com.abdeveloper.maniartradelink.utils.pagination;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.abdeveloper.maniartradelink.home.adapter.ItemListAdapter;
import com.abdeveloper.maniartradelink.orders.adapter.OrderListAdapter;
import com.abdeveloper.maniartradelink.orders.model.OrderModel;
import com.abdeveloper.maniartradelink.retrofit.ServiceAPI;
import com.abdeveloper.maniartradelink.utils.StaticFunctions;
import com.paginate.Paginate;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 13/2/19.
 */

public class CommonPaginationClass implements Paginate.Callbacks {

    //Orders & Quotation
    OrderListAdapter orderListAdapter;
    private ArrayList<OrderModel> orderModelArrayList;

    //Items
    ItemListAdapter itemListAdapter;

    public boolean isQuotation = false;
    public String status = "";


    Context context;
    Paginate paginate;
    int page = 0;
    private static int LIMIT = 10;
    private static int INITIAL_OFFSET = 0;
    public boolean Loading = false;
    public boolean hasLoadedAllItems = false;


    public interface ProductEmptyListener {
        void onNoProductFound();
    }


    ProductEmptyListener productEmptyListener;

    public void setProductEmptyListener(ProductEmptyListener productEmptyListener) {
        this.productEmptyListener = productEmptyListener;
    }

    public CommonPaginationClass(Context context, RecyclerView recyclerView, Object productListAdapter,ArrayList<OrderModel> orderModels,Boolean isQuotation,String orderStatus) {
        this.context = context;
        this.orderModelArrayList = orderModels;
        this.isQuotation = isQuotation;
        this.status = orderStatus;
        paginate = Paginate.with(recyclerView, this)
                .setLoadingTriggerThreshold(2)
                .addLoadingListItem(true)
                .build();
        initialize(productListAdapter);
    }

    private void initialize(Object orderListAdapter) {
        if (orderListAdapter instanceof OrderListAdapter) {
            this.orderListAdapter = (OrderListAdapter) orderListAdapter;
        }
    }


    @Override
    public void onLoadMore() {
        Loading = true;
        if (page == 0) {
            orderModelArrayList.clear();
            getOrders(LIMIT, INITIAL_OFFSET,status);

        } else {
            getOrders(LIMIT, page * LIMIT,status);
        }
        page++;
    }

    @Override
    public boolean isLoading() {
        return Loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return hasLoadedAllItems;
    }

    public void getOrders(int limit, int offset,String status) {

        Call serviceAPI = null;
        if(!isQuotation){
            serviceAPI = new ServiceAPI(context).getOrders(limit,offset,status);
        }else{
            serviceAPI = new ServiceAPI(context).getQuotations(limit,offset);
        }
        //Call server
        serviceAPI.enqueue(new Callback<ArrayList<OrderModel>>() {
            @Override
            public void onResponse(Call<ArrayList<OrderModel>> call, Response<ArrayList<OrderModel>> response) {

                Loading = false;
                if (response.isSuccessful()) {

                    if (response.body().size() > 0) {
                        orderModelArrayList.addAll(response.body());

                        if (orderModelArrayList.size() <= LIMIT) {
                            orderListAdapter.notifyDataSetChanged();
                        } else {
                            orderListAdapter.notifyItemRangeInserted(orderListAdapter.getItemCount(), orderModelArrayList.size() - 1);
                        }
                    } else {
                        hasLoadedAllItems = true;
                        orderListAdapter.notifyDataSetChanged();
                    }


                } else {
                    hasLoadedAllItems = true;
                    orderListAdapter.notifyDataSetChanged();

                    StaticFunctions.commonError(response, context);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<OrderModel>> call, Throwable t) {
                Loading = false;
                hasLoadedAllItems = true;
                orderListAdapter.notifyDataSetChanged();

                StaticFunctions.showAllFailureError(t, context);

            }
        });
    }


}

