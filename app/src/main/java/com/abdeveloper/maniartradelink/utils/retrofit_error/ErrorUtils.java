package com.abdeveloper.maniartradelink.utils.retrofit_error;



import com.abdeveloper.maniartradelink.retrofit.APIError;
import com.abdeveloper.maniartradelink.retrofit.RetroFitServiceGenerator;

import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by root on 18/12/17.
 */

public class ErrorUtils {

    public static APIError parseError(Response<?> response) {
        RetroFitServiceGenerator retroFitServiceGenerator = new RetroFitServiceGenerator();

        Converter<ResponseBody, APIError> converter =
                retroFitServiceGenerator.retrofit.responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error = null;

        try {
            error = converter.convert(response.errorBody());
        } catch (Exception e) {
            return new APIError();
        }

        return error;
    }
}