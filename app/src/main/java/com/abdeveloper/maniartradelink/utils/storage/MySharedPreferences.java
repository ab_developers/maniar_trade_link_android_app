package com.abdeveloper.maniartradelink.utils.storage;

import android.content.Context;
import android.content.SharedPreferences;
/**
 * Created by root on 3/9/17.
 */

public class MySharedPreferences {
    private static MySharedPreferences yourPreference;
    private SharedPreferences sharedPreferences;

    public MySharedPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences("maniartradelink", Context.MODE_PRIVATE);
    }

    public static MySharedPreferences getInstance(Context context) {
        if (yourPreference == null) {
            yourPreference = new MySharedPreferences(context);
        }
        return yourPreference;
    }


    public void ClearAllData() {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.clear();
        prefsEditor.commit();
    }

    public void saveToken(String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("token", value);
        prefsEditor.commit();
    }

    public String getToken() {
        if (sharedPreferences != null) {
            return sharedPreferences.getString("token", "");
        }
        return "";
    }

    public void saveFirebaseTokenID(String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("firebase_token_id", value);
        prefsEditor.commit();
    }

    public String getFirebaseTokenID() {
        if (sharedPreferences != null) {
            return sharedPreferences.getString("firebase_token_id", "");
        }
        return "";
    }

    public void saveFirebaseToken(String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("firebase_token", value);
        prefsEditor.commit();
    }

    public String getFirebaseToken() {
        if (sharedPreferences != null) {
            return sharedPreferences.getString("firebase_token", "");
        }
        return "";
    }
}
