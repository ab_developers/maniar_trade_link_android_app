package com.abdeveloper.maniartradelink.utils.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.abdeveloper.maniartradelink.utils.StaticFunctions;

/**
 * Created by root on 5/2/19.
 */

public class UserProfileLocal {

    SharedPreferences pref;
    Context context;
    String username;
    String email;
    String phone;
    String full_name;
    String user_type;
    String employee_position;
    String join_date;

    public UserProfileLocal(Context context) {
        this.context = context;
        pref = StaticFunctions.getAppSharedPreferences(context);

    }
    public String getUsername() {
        return pref.getString("username","");

    }

    public void setUsername(String username) {
        this.username = username;
        pref.edit().putString("username",username).apply();

    }

    public String getEmployee_position() {
        return pref.getString("employee_position","");
    }

    public void setEmployee_position(String employee_position) {
        this.employee_position = employee_position;
        pref.edit().putString("employee_position",employee_position).apply();

    }

    public String getJoin_date() {
        return pref.getString("join_date","");
    }

    public void setJoin_date(String join_date) {
        this.join_date = join_date;
        pref.edit().putString("join_date",StaticFunctions.currentIST(join_date)).apply();

    }


    public String getEmail() {
        return pref.getString("email","");
    }

    public void setEmail(String email) {
        this.email = email;
        pref.edit().putString("email",email).apply();

    }

    public String getPhone() {
        return pref.getString("phone","");
    }

    public void setPhone(String phone) {
        this.phone = phone;
        pref.edit().putString("phone",phone).apply();

    }

    public String getFull_name() {
        return pref.getString("full_name","");
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
        pref.edit().putString("full_name",full_name).apply();

    }

    public String getUser_type() {
        return pref.getString("user_type","");
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
        pref.edit().putString("user_type",user_type).apply();

    }

}
